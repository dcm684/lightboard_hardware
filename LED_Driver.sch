EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74HC4094 U2
U 1 1 54DE5975
P 2000 2100
F 0 "U2" H 2400 2700 70  0000 R CNN
F 1 "74HC4094" H 2000 1400 70  0000 C CNN
F 2 "SMD_Packages:SO-16-N" H 2000 2100 60  0001 C CNN
F 3 "" H 2000 2100 60  0000 C CNN
	1    2000 2100
	1    0    0    -1  
$EndComp
$Comp
L 74HC4094 U3
U 1 1 54DE597C
P 2050 3550
F 0 "U3" H 2450 4150 70  0000 R CNN
F 1 "74HC4094" H 2050 2850 70  0000 C CNN
F 2 "SMD_Packages:SO-16-N" H 2050 3550 60  0001 C CNN
F 3 "" H 2050 3550 60  0000 C CNN
	1    2050 3550
	1    0    0    -1  
$EndComp
$Comp
L 74HC4094 U4
U 1 1 54DE5983
P 2050 4950
F 0 "U4" H 2450 5550 70  0000 R CNN
F 1 "74HC4094" H 2050 4250 70  0000 C CNN
F 2 "SMD_Packages:SO-16-N" H 2050 4950 60  0001 C CNN
F 3 "" H 2050 4950 60  0000 C CNN
	1    2050 4950
	1    0    0    -1  
$EndComp
Text HLabel 950  6200 0    60   Input ~ 0
Data_0
Text HLabel 950  6300 0    60   Input ~ 0
Data_1
Text HLabel 950  6700 0    60   Input ~ 0
Clock
Text HLabel 950  6600 0    60   Input ~ 0
Strobe
Text HLabel 950  6500 0    60   Input ~ 0
OE
NoConn ~ 2700 1650
NoConn ~ 2700 1750
NoConn ~ 2750 3100
NoConn ~ 2750 3200
NoConn ~ 2750 4500
NoConn ~ 2750 4600
$Comp
L +5V #PWR018
U 1 1 54DE6385
P 5200 1400
F 0 "#PWR018" H 5200 1490 20  0001 C CNN
F 1 "+5V" H 5200 1490 30  0000 C CNN
F 2 "" H 5200 1400 60  0000 C CNN
F 3 "" H 5200 1400 60  0000 C CNN
	1    5200 1400
	1    0    0    -1  
$EndComp
Text Label 5300 1200 0    60   ~ 0
Out_X0
Text Label 5300 1900 0    60   ~ 0
Out_X1
Text Label 5300 2600 0    60   ~ 0
Out_X2
Text Label 5300 3300 0    60   ~ 0
Out_X3
Text Label 5300 4000 0    60   ~ 0
Out_X4
Text Label 5300 4700 0    60   ~ 0
Out_X5
Text Label 5300 5400 0    60   ~ 0
Out_X6
Text Label 5300 6100 0    60   ~ 0
Out_X7
Text Label 5300 6800 0    60   ~ 0
Out_X8
Text Label 5300 7500 0    60   ~ 0
Out_X9
Text Label 3800 1300 2    60   ~ 0
Shifted_X0
Text Label 3800 1400 2    60   ~ 0
Shifted_X1
Text Label 3800 3200 2    60   ~ 0
Shifted_X2
Text Label 3800 3300 2    60   ~ 0
Shifted_X3
Text Label 3800 3400 2    60   ~ 0
Shifted_X4
Text Label 3800 3500 2    60   ~ 0
Shifted_X5
Text Label 3800 6000 2    60   ~ 0
Shifted_X6
Text Label 3800 6100 2    60   ~ 0
Shifted_X7
Text Label 3800 6200 2    60   ~ 0
Shifted_X8
Text Label 3800 6300 2    60   ~ 0
Shifted_X9
Text Label 2750 1950 0    60   ~ 0
Shifted_X0
Text Label 2750 2050 0    60   ~ 0
Shifted_X1
Text Label 2750 2150 0    60   ~ 0
Shifted_X2
Text Label 2750 2250 0    60   ~ 0
Shifted_X3
Text Label 2750 2350 0    60   ~ 0
Shifted_X4
Text Label 2750 2450 0    60   ~ 0
Shifted_X5
Text Label 2750 2550 0    60   ~ 0
Shifted_X6
Text Label 2800 3400 0    60   ~ 0
Shifted_X7
Text Label 2800 3500 0    60   ~ 0
Shifted_X8
Text Label 2800 3600 0    60   ~ 0
Shifted_X9
Text Label 2800 3700 0    60   ~ 0
Shifted_Y0
Text Label 2800 3800 0    60   ~ 0
Shifted_Y1
Text Label 2800 3900 0    60   ~ 0
Shifted_Y2
Text Label 2800 4800 0    60   ~ 0
Shifted_Y3
Text Label 2800 4900 0    60   ~ 0
Shifted_Y4
Text Label 2800 5000 0    60   ~ 0
Shifted_Y5
Text Label 2800 5100 0    60   ~ 0
Shifted_Y6
Text Label 2800 5200 0    60   ~ 0
Shifted_Y7
Text Label 2800 5300 0    60   ~ 0
Shifted_Y8
Text Label 2800 5400 0    60   ~ 0
Shifted_Y9
$Comp
L GND #PWR019
U 1 1 54E21E3F
P 7500 1400
F 0 "#PWR019" H 7500 1400 30  0001 C CNN
F 1 "GND" H 7500 1330 30  0001 C CNN
F 2 "" H 7500 1400 60  0000 C CNN
F 3 "" H 7500 1400 60  0000 C CNN
	1    7500 1400
	-1   0    0    -1  
$EndComp
Text Label 6100 1750 2    60   ~ 0
Shifted_Y0
Text Label 6100 1850 2    60   ~ 0
Shifted_Y1
Text Label 6100 1950 2    60   ~ 0
Shifted_Y2
Text Label 6100 2050 2    60   ~ 0
Shifted_Y3
Text Label 6100 4550 2    60   ~ 0
Shifted_Y4
Text Label 6100 4650 2    60   ~ 0
Shifted_Y5
Text Label 6100 4750 2    60   ~ 0
Shifted_Y6
Text Label 6100 4850 2    60   ~ 0
Shifted_Y7
Text Label 8100 3300 2    60   ~ 0
Shifted_Y8
Text Label 8100 3400 2    60   ~ 0
Shifted_Y9
Text Label 8150 1750 0    60   ~ 0
Out_Y0
Text Label 8150 1850 0    60   ~ 0
Out_Y1
Text Label 8150 1950 0    60   ~ 0
Out_Y2
Text Label 8150 2050 0    60   ~ 0
Out_Y3
Text Label 8150 4550 0    60   ~ 0
Out_Y4
Text Label 8150 4650 0    60   ~ 0
Out_Y5
Text Label 8150 4750 0    60   ~ 0
Out_Y6
Text Label 8150 4850 0    60   ~ 0
Out_Y7
Text Label 10150 3300 0    60   ~ 0
Out_Y8
Text Label 10150 3400 0    60   ~ 0
Out_Y9
$Comp
L CONN_02X11 J5
U 1 1 54E40963
P 9450 5600
F 0 "J5" H 9450 6200 50  0000 C CNN
F 1 "CONN_02X11" V 9450 5600 50  0000 C CNN
F 2 "Custom_Connectors:TE_794107-1" H 9450 4400 60  0001 C CNN
F 3 "" H 9450 4400 60  0000 C CNN
	1    9450 5600
	1    0    0    -1  
$EndComp
Text Label 9150 5100 2    60   ~ 0
Out_Y0
Text Label 9150 5200 2    60   ~ 0
Out_Y1
Text Label 9150 5300 2    60   ~ 0
Out_Y2
Text Label 9150 5400 2    60   ~ 0
Out_Y3
Text Label 9150 5500 2    60   ~ 0
Out_Y4
Text Label 9150 5600 2    60   ~ 0
Out_Y5
Text Label 9150 5700 2    60   ~ 0
Out_Y6
Text Label 9150 5800 2    60   ~ 0
Out_Y7
Text Label 9150 5900 2    60   ~ 0
Out_Y8
Text Label 9150 6000 2    60   ~ 0
Out_Y9
Text Label 9750 5100 0    60   ~ 0
Out_X0
Text Label 9750 5200 0    60   ~ 0
Out_X1
Text Label 9750 5300 0    60   ~ 0
Out_X2
Text Label 9750 5400 0    60   ~ 0
Out_X3
Text Label 9750 5500 0    60   ~ 0
Out_X4
Text Label 9750 5600 0    60   ~ 0
Out_X5
Text Label 9750 5700 0    60   ~ 0
Out_X6
Text Label 9750 5800 0    60   ~ 0
Out_X7
Text Label 9750 5900 0    60   ~ 0
Out_X8
Text Label 9750 6000 0    60   ~ 0
Out_X9
NoConn ~ 9700 6100
$Comp
L +5V #PWR020
U 1 1 54E41632
P 5200 700
F 0 "#PWR020" H 5200 790 20  0001 C CNN
F 1 "+5V" H 5200 790 30  0000 C CNN
F 2 "" H 5200 700 60  0000 C CNN
F 3 "" H 5200 700 60  0000 C CNN
	1    5200 700 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR021
U 1 1 54E41691
P 5200 2100
F 0 "#PWR021" H 5200 2190 20  0001 C CNN
F 1 "+5V" H 5200 2190 30  0000 C CNN
F 2 "" H 5200 2100 60  0000 C CNN
F 3 "" H 5200 2100 60  0000 C CNN
	1    5200 2100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR022
U 1 1 54E427F4
P 5200 2800
F 0 "#PWR022" H 5200 2890 20  0001 C CNN
F 1 "+5V" H 5200 2890 30  0000 C CNN
F 2 "" H 5200 2800 60  0000 C CNN
F 3 "" H 5200 2800 60  0000 C CNN
	1    5200 2800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR023
U 1 1 54E42827
P 5200 3500
F 0 "#PWR023" H 5200 3590 20  0001 C CNN
F 1 "+5V" H 5200 3590 30  0000 C CNN
F 2 "" H 5200 3500 60  0000 C CNN
F 3 "" H 5200 3500 60  0000 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 54E4395E
P 5200 4200
F 0 "#PWR024" H 5200 4290 20  0001 C CNN
F 1 "+5V" H 5200 4290 30  0000 C CNN
F 2 "" H 5200 4200 60  0000 C CNN
F 3 "" H 5200 4200 60  0000 C CNN
	1    5200 4200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR025
U 1 1 54E444BD
P 5200 4900
F 0 "#PWR025" H 5200 4990 20  0001 C CNN
F 1 "+5V" H 5200 4990 30  0000 C CNN
F 2 "" H 5200 4900 60  0000 C CNN
F 3 "" H 5200 4900 60  0000 C CNN
	1    5200 4900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR026
U 1 1 54E444F0
P 5200 5600
F 0 "#PWR026" H 5200 5690 20  0001 C CNN
F 1 "+5V" H 5200 5690 30  0000 C CNN
F 2 "" H 5200 5600 60  0000 C CNN
F 3 "" H 5200 5600 60  0000 C CNN
	1    5200 5600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR027
U 1 1 54E44523
P 5200 6300
F 0 "#PWR027" H 5200 6390 20  0001 C CNN
F 1 "+5V" H 5200 6390 30  0000 C CNN
F 2 "" H 5200 6300 60  0000 C CNN
F 3 "" H 5200 6300 60  0000 C CNN
	1    5200 6300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR028
U 1 1 54E45132
P 5200 7000
F 0 "#PWR028" H 5200 7090 20  0001 C CNN
F 1 "+5V" H 5200 7090 30  0000 C CNN
F 2 "" H 5200 7000 60  0000 C CNN
F 3 "" H 5200 7000 60  0000 C CNN
	1    5200 7000
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q6
U 1 1 54E46859
P 7450 1200
F 0 "Q6" H 7350 1500 40  0000 C CNN
F 1 "Si1922EDH" H 7250 1550 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 1200 60  0001 C CNN
F 3 "" H 7450 1200 60  0000 C CNN
	1    7450 1200
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q6
U 2 1 54E47FC3
P 7450 1900
F 0 "Q6" H 7350 2200 40  0000 C CNN
F 1 "Si1922EDH" H 7250 2250 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 1900 60  0001 C CNN
F 3 "" H 7450 1900 60  0000 C CNN
	2    7450 1900
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q7
U 1 1 54E48027
P 7450 2600
F 0 "Q7" H 7350 2900 40  0000 C CNN
F 1 "Si1922EDH" H 7250 2950 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 2600 60  0001 C CNN
F 3 "" H 7450 2600 60  0000 C CNN
	1    7450 2600
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q7
U 2 1 54E48074
P 7450 3300
F 0 "Q7" H 7350 3600 40  0000 C CNN
F 1 "Si1922EDH" H 7250 3650 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 3300 60  0001 C CNN
F 3 "" H 7450 3300 60  0000 C CNN
	2    7450 3300
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q8
U 1 1 54E480C0
P 7450 4000
F 0 "Q8" H 7350 4300 40  0000 C CNN
F 1 "Si1922EDH" H 7250 4350 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 4000 60  0001 C CNN
F 3 "" H 7450 4000 60  0000 C CNN
	1    7450 4000
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q8
U 2 1 54E4810D
P 7450 4700
F 0 "Q8" H 7350 5000 40  0000 C CNN
F 1 "Si1922EDH" H 7250 5050 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 4700 60  0001 C CNN
F 3 "" H 7450 4700 60  0000 C CNN
	2    7450 4700
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q9
U 1 1 54E48171
P 7450 5400
F 0 "Q9" H 7350 5700 40  0000 C CNN
F 1 "Si1922EDH" H 7250 5750 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 5400 60  0001 C CNN
F 3 "" H 7450 5400 60  0000 C CNN
	1    7450 5400
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q9
U 2 1 54E481DC
P 7450 6100
F 0 "Q9" H 7350 6400 40  0000 C CNN
F 1 "Si1922EDH" H 7250 6450 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 7450 6100 60  0001 C CNN
F 3 "" H 7450 6100 60  0000 C CNN
	2    7450 6100
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q10
U 1 1 54E48DD8
P 9450 3150
F 0 "Q10" H 9350 3450 40  0000 C CNN
F 1 "Si1922EDH" H 9250 3500 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 9450 3150 60  0001 C CNN
F 3 "" H 9450 3150 60  0000 C CNN
	1    9450 3150
	1    0    0    -1  
$EndComp
$Comp
L Si1922EDH Q10
U 2 1 54E48E2F
P 9450 3850
F 0 "Q10" H 9350 4150 40  0000 C CNN
F 1 "Si1922EDH" H 9250 4200 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 9450 3850 60  0001 C CNN
F 3 "" H 9450 3850 60  0000 C CNN
	2    9450 3850
	1    0    0    -1  
$EndComp
$Comp
L Si11967DH Q1
U 1 1 54E49326
P 5150 900
F 0 "Q1" H 5050 1000 40  0000 C CNN
F 1 "Si11967DH" H 4950 1050 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 900 60  0001 C CNN
F 3 "" H 5150 900 60  0000 C CNN
	1    5150 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 54E49DFD
P 7500 2100
F 0 "#PWR029" H 7500 2100 30  0001 C CNN
F 1 "GND" H 7500 2030 30  0001 C CNN
F 2 "" H 7500 2100 60  0000 C CNN
F 3 "" H 7500 2100 60  0000 C CNN
	1    7500 2100
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q1
U 2 1 54E49E06
P 5150 1600
F 0 "Q1" H 5050 1700 40  0000 C CNN
F 1 "Si11967DH" H 4950 1750 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 1600 60  0001 C CNN
F 3 "" H 5150 1600 60  0000 C CNN
	2    5150 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR030
U 1 1 54E49F82
P 7500 2800
F 0 "#PWR030" H 7500 2800 30  0001 C CNN
F 1 "GND" H 7500 2730 30  0001 C CNN
F 2 "" H 7500 2800 60  0000 C CNN
F 3 "" H 7500 2800 60  0000 C CNN
	1    7500 2800
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q2
U 1 1 54E49F8B
P 5150 2300
F 0 "Q2" H 5050 2400 40  0000 C CNN
F 1 "Si11967DH" H 4950 2450 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 2300 60  0001 C CNN
F 3 "" H 5150 2300 60  0000 C CNN
	1    5150 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 54E49F97
P 7500 3500
F 0 "#PWR031" H 7500 3500 30  0001 C CNN
F 1 "GND" H 7500 3430 30  0001 C CNN
F 2 "" H 7500 3500 60  0000 C CNN
F 3 "" H 7500 3500 60  0000 C CNN
	1    7500 3500
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q2
U 2 1 54E49FA0
P 5150 3000
F 0 "Q2" H 5050 3100 40  0000 C CNN
F 1 "Si11967DH" H 4950 3150 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 3000 60  0001 C CNN
F 3 "" H 5150 3000 60  0000 C CNN
	2    5150 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR032
U 1 1 54E4AEA8
P 7500 4200
F 0 "#PWR032" H 7500 4200 30  0001 C CNN
F 1 "GND" H 7500 4130 30  0001 C CNN
F 2 "" H 7500 4200 60  0000 C CNN
F 3 "" H 7500 4200 60  0000 C CNN
	1    7500 4200
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q3
U 1 1 54E4AEB1
P 5150 3700
F 0 "Q3" H 5050 3800 40  0000 C CNN
F 1 "Si11967DH" H 4950 3850 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 3700 60  0001 C CNN
F 3 "" H 5150 3700 60  0000 C CNN
	1    5150 3700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR033
U 1 1 54E4AEBD
P 7500 4900
F 0 "#PWR033" H 7500 4900 30  0001 C CNN
F 1 "GND" H 7500 4830 30  0001 C CNN
F 2 "" H 7500 4900 60  0000 C CNN
F 3 "" H 7500 4900 60  0000 C CNN
	1    7500 4900
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q3
U 2 1 54E4AEC6
P 5150 4400
F 0 "Q3" H 5050 4500 40  0000 C CNN
F 1 "Si11967DH" H 4950 4550 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 4400 60  0001 C CNN
F 3 "" H 5150 4400 60  0000 C CNN
	2    5150 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR034
U 1 1 54E4B79A
P 7500 5600
F 0 "#PWR034" H 7500 5600 30  0001 C CNN
F 1 "GND" H 7500 5530 30  0001 C CNN
F 2 "" H 7500 5600 60  0000 C CNN
F 3 "" H 7500 5600 60  0000 C CNN
	1    7500 5600
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q4
U 1 1 54E4B7A3
P 5150 5100
F 0 "Q4" H 5050 5200 40  0000 C CNN
F 1 "Si11967DH" H 4950 5250 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 5100 60  0001 C CNN
F 3 "" H 5150 5100 60  0000 C CNN
	1    5150 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR035
U 1 1 54E4B7AF
P 7500 6300
F 0 "#PWR035" H 7500 6300 30  0001 C CNN
F 1 "GND" H 7500 6230 30  0001 C CNN
F 2 "" H 7500 6300 60  0000 C CNN
F 3 "" H 7500 6300 60  0000 C CNN
	1    7500 6300
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q4
U 2 1 54E4B7B8
P 5150 5800
F 0 "Q4" H 5050 5900 40  0000 C CNN
F 1 "Si11967DH" H 4950 5950 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 5800 60  0001 C CNN
F 3 "" H 5150 5800 60  0000 C CNN
	2    5150 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR036
U 1 1 54E4BE36
P 9500 3350
F 0 "#PWR036" H 9500 3350 30  0001 C CNN
F 1 "GND" H 9500 3280 30  0001 C CNN
F 2 "" H 9500 3350 60  0000 C CNN
F 3 "" H 9500 3350 60  0000 C CNN
	1    9500 3350
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q5
U 1 1 54E4BE3F
P 5150 6500
F 0 "Q5" H 5050 6600 40  0000 C CNN
F 1 "Si11967DH" H 4950 6650 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 6500 60  0001 C CNN
F 3 "" H 5150 6500 60  0000 C CNN
	1    5150 6500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 54E4BE4B
P 9500 4050
F 0 "#PWR037" H 9500 4050 30  0001 C CNN
F 1 "GND" H 9500 3980 30  0001 C CNN
F 2 "" H 9500 4050 60  0000 C CNN
F 3 "" H 9500 4050 60  0000 C CNN
	1    9500 4050
	-1   0    0    -1  
$EndComp
$Comp
L Si11967DH Q5
U 2 1 54E4BE54
P 5150 7200
F 0 "Q5" H 5050 7300 40  0000 C CNN
F 1 "Si11967DH" H 4950 7350 40  0000 C CNN
F 2 "Personal_SMD:SC-70_LittleFoot" H 5150 7200 60  0001 C CNN
F 3 "" H 5150 7200 60  0000 C CNN
	2    5150 7200
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP3
U 1 1 54E4EDE0
P 7900 2100
F 0 "RP3" H 7900 2550 40  0000 C CNN
F 1 "22" H 7900 2050 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 7900 2100 60  0001 C CNN
F 3 "" H 7900 2100 60  0000 C CNN
	1    7900 2100
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP4
U 1 1 54E50483
P 7900 4900
F 0 "RP4" H 7900 5350 40  0000 C CNN
F 1 "22" H 7900 4850 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 7900 4900 60  0001 C CNN
F 3 "" H 7900 4900 60  0000 C CNN
	1    7900 4900
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP6
U 1 1 54E51939
P 9900 3650
F 0 "RP6" H 9900 4100 40  0000 C CNN
F 1 "22" H 9900 3600 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 9900 3650 60  0001 C CNN
F 3 "" H 9900 3650 60  0000 C CNN
	1    9900 3650
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP1
U 1 1 54E54278
P 6750 2100
F 0 "RP1" H 6750 2550 40  0000 C CNN
F 1 "180" H 6750 2050 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 6750 2100 60  0001 C CNN
F 3 "" H 6750 2100 60  0000 C CNN
	1    6750 2100
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP2
U 1 1 54E55E5D
P 6750 4900
F 0 "RP2" H 6750 5350 40  0000 C CNN
F 1 "180" H 6750 4850 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 6750 4900 60  0001 C CNN
F 3 "" H 6750 4900 60  0000 C CNN
	1    6750 4900
	1    0    0    -1  
$EndComp
NoConn ~ 9700 3500
NoConn ~ 9700 3600
NoConn ~ 10100 3500
NoConn ~ 10100 3600
NoConn ~ 8950 3500
NoConn ~ 8950 3600
$Comp
L VCC #PWR038
U 1 1 54E5C38B
P 1650 850
F 0 "#PWR038" H 1650 950 30  0001 C CNN
F 1 "VCC" H 1650 950 30  0000 C CNN
F 2 "" H 1650 850 60  0000 C CNN
F 3 "" H 1650 850 60  0000 C CNN
	1    1650 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR039
U 1 1 54E5C39F
P 1650 1350
F 0 "#PWR039" H 1650 1350 30  0001 C CNN
F 1 "GND" H 1650 1280 30  0001 C CNN
F 2 "" H 1650 1350 60  0000 C CNN
F 3 "" H 1650 1350 60  0000 C CNN
	1    1650 1350
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 54E5C453
P 1650 1100
F 0 "C5" H 1650 1200 40  0000 L CNN
F 1 "0.01u" H 1656 1015 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1688 950 30  0001 C CNN
F 3 "" H 1650 1100 60  0000 C CNN
	1    1650 1100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR040
U 1 1 54E5C857
P 1950 850
F 0 "#PWR040" H 1950 950 30  0001 C CNN
F 1 "VCC" H 1950 950 30  0000 C CNN
F 2 "" H 1950 850 60  0000 C CNN
F 3 "" H 1950 850 60  0000 C CNN
	1    1950 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR041
U 1 1 54E5C85D
P 1950 1350
F 0 "#PWR041" H 1950 1350 30  0001 C CNN
F 1 "GND" H 1950 1280 30  0001 C CNN
F 2 "" H 1950 1350 60  0000 C CNN
F 3 "" H 1950 1350 60  0000 C CNN
	1    1950 1350
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 54E5C863
P 1950 1100
F 0 "C6" H 1950 1200 40  0000 L CNN
F 1 "0.01u" H 1956 1015 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1988 950 30  0001 C CNN
F 3 "" H 1950 1100 60  0000 C CNN
	1    1950 1100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR042
U 1 1 54E5C899
P 2250 850
F 0 "#PWR042" H 2250 950 30  0001 C CNN
F 1 "VCC" H 2250 950 30  0000 C CNN
F 2 "" H 2250 850 60  0000 C CNN
F 3 "" H 2250 850 60  0000 C CNN
	1    2250 850 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR043
U 1 1 54E5C89F
P 2250 1350
F 0 "#PWR043" H 2250 1350 30  0001 C CNN
F 1 "GND" H 2250 1280 30  0001 C CNN
F 2 "" H 2250 1350 60  0000 C CNN
F 3 "" H 2250 1350 60  0000 C CNN
	1    2250 1350
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 54E5C8A5
P 2250 1100
F 0 "C7" H 2250 1200 40  0000 L CNN
F 1 "0.01u" H 2256 1015 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2288 950 30  0001 C CNN
F 3 "" H 2250 1100 60  0000 C CNN
	1    2250 1100
	1    0    0    -1  
$EndComp
NoConn ~ 9200 6100
$Comp
L CONN_02X04 J4
U 1 1 54E856A1
P 2300 6900
F 0 "J4" H 2300 7150 50  0000 C CNN
F 1 "External Control" H 2300 6650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04" H 2300 5700 60  0001 C CNN
F 3 "" H 2300 5700 60  0000 C CNN
	1    2300 6900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR044
U 1 1 54E85D06
P 1950 6050
F 0 "#PWR044" H 1950 6140 20  0001 C CNN
F 1 "+5V" H 1950 6140 30  0000 C CNN
F 2 "" H 1950 6050 60  0000 C CNN
F 3 "" H 1950 6050 60  0000 C CNN
	1    1950 6050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR045
U 1 1 54E85D1A
P 2650 7100
F 0 "#PWR045" H 2650 7100 30  0001 C CNN
F 1 "GND" H 2650 7030 30  0001 C CNN
F 2 "" H 2650 7100 60  0000 C CNN
F 3 "" H 2650 7100 60  0000 C CNN
	1    2650 7100
	1    0    0    -1  
$EndComp
Text Label 1950 6850 2    60   ~ 0
Data_1
Text Label 1950 6950 2    60   ~ 0
Strobe
Text Label 1950 7050 2    60   ~ 0
Clock
Text Label 2650 6750 0    60   ~ 0
Data_0
Text Label 2650 6850 0    60   ~ 0
Data_2
Text Label 2650 6950 0    60   ~ 0
OE
Text HLabel 950  6400 0    60   Input ~ 0
Data_2
Text Label 800  4500 2    60   ~ 0
Data_2
Text Label 800  3100 2    60   ~ 0
Data_1
Text Label 800  2100 2    60   ~ 0
OE
Text Label 800  2000 2    60   ~ 0
Strobe
Text Label 800  1750 2    60   ~ 0
Clock
Text Label 800  1650 2    60   ~ 0
Data_0
Text Label 1100 6200 0    60   ~ 0
Data_0
Text Label 1100 6300 0    60   ~ 0
Data_1
Text Label 1100 6400 0    60   ~ 0
Data_2
Text Label 1100 6500 0    60   ~ 0
OE
Text Label 1100 6600 0    60   ~ 0
Strobe
Text Label 1100 6700 0    60   ~ 0
Clock
$Comp
L JUMPER JP1
U 1 1 54E870A9
P 1950 6400
F 0 "JP1" H 1950 6550 60  0000 C CNN
F 1 "Enable External" H 1950 6320 40  0000 C CNN
F 2 "Custom_Jumpers:JMP_SLDR-2N" H 1950 6400 60  0001 C CNN
F 3 "" H 1950 6400 60  0000 C CNN
	1    1950 6400
	0    1    1    0   
$EndComp
NoConn ~ 2800 4000
NoConn ~ 2800 4100
NoConn ~ 2800 5500
$Comp
L R_PACK4 RP10
U 1 1 550AC773
P 4450 1450
F 0 "RP10" H 4450 1900 40  0000 C CNN
F 1 "180" H 4450 1400 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 4450 1450 60  0001 C CNN
F 3 "" H 4450 1450 60  0000 C CNN
	1    4450 1450
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP7
U 1 1 550AC85F
P 3850 850
F 0 "RP7" H 3850 1300 40  0000 C CNN
F 1 "100k" H 3850 800 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 3850 850 60  0001 C CNN
F 3 "" H 3850 850 60  0000 C CNN
	1    3850 850 
	0    1    -1   0   
$EndComp
$Comp
L R_PACK4 RP11
U 1 1 550AF1F1
P 4450 3550
F 0 "RP11" H 4450 4000 40  0000 C CNN
F 1 "180" H 4450 3500 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 4450 3550 60  0001 C CNN
F 3 "" H 4450 3550 60  0000 C CNN
	1    4450 3550
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP8
U 1 1 550AF1F7
P 3850 2950
F 0 "RP8" H 3850 3400 40  0000 C CNN
F 1 "100k" H 3850 2900 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 3850 2950 60  0001 C CNN
F 3 "" H 3850 2950 60  0000 C CNN
	1    3850 2950
	0    1    -1   0   
$EndComp
$Comp
L R_PACK4 RP12
U 1 1 550AF955
P 4450 6350
F 0 "RP12" H 4450 6800 40  0000 C CNN
F 1 "180" H 4450 6300 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 4450 6350 60  0001 C CNN
F 3 "" H 4450 6350 60  0000 C CNN
	1    4450 6350
	1    0    0    -1  
$EndComp
$Comp
L R_PACK4 RP9
U 1 1 550AF95B
P 3850 5750
F 0 "RP9" H 3850 6200 40  0000 C CNN
F 1 "100k" H 3850 5700 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 3850 5750 60  0001 C CNN
F 3 "" H 3850 5750 60  0000 C CNN
	1    3850 5750
	0    1    -1   0   
$EndComp
$Comp
L +5V #PWR046
U 1 1 550B0492
P 4050 500
F 0 "#PWR046" H 4050 590 20  0001 C CNN
F 1 "+5V" H 4050 590 30  0000 C CNN
F 2 "" H 4050 500 60  0000 C CNN
F 3 "" H 4050 500 60  0000 C CNN
	1    4050 500 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR047
U 1 1 550B05C5
P 4050 2600
F 0 "#PWR047" H 4050 2690 20  0001 C CNN
F 1 "+5V" H 4050 2690 30  0000 C CNN
F 2 "" H 4050 2600 60  0000 C CNN
F 3 "" H 4050 2600 60  0000 C CNN
	1    4050 2600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR048
U 1 1 550B06A1
P 4050 5400
F 0 "#PWR048" H 4050 5490 20  0001 C CNN
F 1 "+5V" H 4050 5490 30  0000 C CNN
F 2 "" H 4050 5400 60  0000 C CNN
F 3 "" H 4050 5400 60  0000 C CNN
	1    4050 5400
	1    0    0    -1  
$EndComp
NoConn ~ 2750 2650
$Comp
L R_PACK4 RP13
U 1 1 550B5678
P 6550 2300
F 0 "RP13" H 6550 2750 40  0000 C CNN
F 1 "100k" H 6550 2250 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 6550 2300 60  0001 C CNN
F 3 "" H 6550 2300 60  0000 C CNN
	1    6550 2300
	0    -1   1    0   
$EndComp
$Comp
L R_PACK4 RP14
U 1 1 550B575E
P 6550 5100
F 0 "RP14" H 6550 5550 40  0000 C CNN
F 1 "100k" H 6550 5050 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 6550 5100 60  0001 C CNN
F 3 "" H 6550 5100 60  0000 C CNN
	1    6550 5100
	0    -1   1    0   
$EndComp
$Comp
L R_PACK4 RP15
U 1 1 550B59CC
P 8550 3850
F 0 "RP15" H 8550 4300 40  0000 C CNN
F 1 "100k" H 8550 3800 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 8550 3850 60  0001 C CNN
F 3 "" H 8550 3850 60  0000 C CNN
	1    8550 3850
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR049
U 1 1 550B7E72
P 6350 2600
F 0 "#PWR049" H 6350 2600 30  0001 C CNN
F 1 "GND" H 6350 2530 30  0001 C CNN
F 2 "" H 6350 2600 60  0000 C CNN
F 3 "" H 6350 2600 60  0000 C CNN
	1    6350 2600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR050
U 1 1 550B80DF
P 6350 5400
F 0 "#PWR050" H 6350 5400 30  0001 C CNN
F 1 "GND" H 6350 5330 30  0001 C CNN
F 2 "" H 6350 5400 60  0000 C CNN
F 3 "" H 6350 5400 60  0000 C CNN
	1    6350 5400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 550B8585
P 8350 4150
F 0 "#PWR051" H 8350 4150 30  0001 C CNN
F 1 "GND" H 8350 4080 30  0001 C CNN
F 2 "" H 8350 4150 60  0000 C CNN
F 3 "" H 8350 4150 60  0000 C CNN
	1    8350 4150
	-1   0    0    -1  
$EndComp
$Comp
L R_PACK4 RP5
U 1 1 54E5AA9E
P 8750 3650
F 0 "RP5" H 8750 4100 40  0000 C CNN
F 1 "180" H 8750 3600 40  0000 C CNN
F 2 "Custom_SMD:1206_Array_of_4" H 8750 3650 60  0001 C CNN
F 3 "" H 8750 3650 60  0000 C CNN
	1    8750 3650
	1    0    0    -1  
$EndComp
Connection ~ 4200 6300
Connection ~ 4100 6200
Connection ~ 8500 3300
Wire Wire Line
	8500 3650 8500 3300
Connection ~ 8400 3400
Wire Wire Line
	8400 3650 8400 3400
Wire Wire Line
	8300 3500 8300 3650
Wire Wire Line
	8200 3600 8200 3650
Connection ~ 6200 4850
Wire Wire Line
	6200 4900 6200 4850
Connection ~ 6300 4750
Wire Wire Line
	6300 4900 6300 4750
Connection ~ 6400 4650
Wire Wire Line
	6400 4900 6400 4650
Connection ~ 6500 4550
Wire Wire Line
	6500 4900 6500 4550
Connection ~ 6500 1750
Wire Wire Line
	6500 2100 6500 1750
Connection ~ 6400 1850
Wire Wire Line
	6400 2100 6400 1850
Connection ~ 6300 1950
Wire Wire Line
	6300 2100 6300 1950
Connection ~ 6200 2050
Wire Wire Line
	6200 2100 6200 2050
Wire Wire Line
	3800 6300 4250 6300
Wire Wire Line
	4200 5950 4200 6300
Wire Wire Line
	3800 6200 4250 6200
Wire Wire Line
	4100 5950 4100 6200
Connection ~ 4000 6100
Wire Wire Line
	4000 5950 4000 6100
Connection ~ 3900 6000
Wire Wire Line
	3900 5950 3900 6000
Connection ~ 4200 3500
Wire Wire Line
	4200 3150 4200 3500
Connection ~ 4100 3400
Wire Wire Line
	4100 3150 4100 3400
Connection ~ 4000 3300
Wire Wire Line
	4000 3150 4000 3300
Connection ~ 3900 3200
Wire Wire Line
	3900 3150 3900 3200
Connection ~ 4200 1400
Wire Wire Line
	4200 1050 4200 1400
Connection ~ 4100 1300
Wire Wire Line
	4100 1050 4100 1300
Connection ~ 4000 1200
Wire Wire Line
	4000 1050 4000 1200
Connection ~ 3900 1100
Wire Wire Line
	3900 1050 3900 1100
Wire Wire Line
	8300 3500 8550 3500
Wire Wire Line
	8200 3600 8550 3600
Connection ~ 8350 4100
Wire Wire Line
	8350 4100 8350 4150
Connection ~ 6350 5350
Wire Wire Line
	6350 5350 6350 5400
Connection ~ 6350 2550
Wire Wire Line
	6350 2550 6350 2600
Connection ~ 8400 4100
Wire Wire Line
	8500 4100 8500 4050
Connection ~ 8300 4100
Wire Wire Line
	8400 4100 8400 4050
Wire Wire Line
	8300 4100 8300 4050
Wire Wire Line
	8200 4100 8500 4100
Wire Wire Line
	8200 4050 8200 4100
Connection ~ 6400 5350
Wire Wire Line
	6500 5350 6500 5300
Connection ~ 6300 5350
Wire Wire Line
	6400 5350 6400 5300
Wire Wire Line
	6300 5350 6300 5300
Wire Wire Line
	6200 5350 6500 5350
Wire Wire Line
	6200 5300 6200 5350
Connection ~ 6400 2550
Wire Wire Line
	6500 2550 6500 2500
Connection ~ 6300 2550
Wire Wire Line
	6400 2550 6400 2500
Wire Wire Line
	6300 2550 6300 2500
Wire Wire Line
	6200 2550 6500 2550
Wire Wire Line
	6200 2500 6200 2550
Connection ~ 4050 5500
Wire Wire Line
	4050 5500 4050 5400
Connection ~ 4100 5500
Wire Wire Line
	4200 5500 4200 5550
Connection ~ 4000 5500
Wire Wire Line
	4100 5500 4100 5550
Wire Wire Line
	4000 5500 4000 5550
Wire Wire Line
	3900 5500 4200 5500
Wire Wire Line
	3900 5550 3900 5500
Connection ~ 4050 2700
Wire Wire Line
	4050 2700 4050 2600
Connection ~ 4100 2700
Wire Wire Line
	4200 2700 4200 2750
Connection ~ 4000 2700
Wire Wire Line
	4100 2700 4100 2750
Wire Wire Line
	4000 2700 4000 2750
Wire Wire Line
	3900 2700 4200 2700
Wire Wire Line
	3900 2750 3900 2700
Connection ~ 4050 600 
Wire Wire Line
	4050 600  4050 500 
Connection ~ 4100 600 
Wire Wire Line
	4200 600  4200 650 
Connection ~ 4000 600 
Wire Wire Line
	4100 600  4100 650 
Wire Wire Line
	4000 600  4000 650 
Wire Wire Line
	3900 600  4200 600 
Wire Wire Line
	3900 650  3900 600 
Wire Wire Line
	4700 7200 4900 7200
Wire Wire Line
	4750 6500 4900 6500
Wire Wire Line
	4700 5100 4900 5100
Wire Wire Line
	4650 1400 4750 1400
Wire Wire Line
	4700 2300 4900 2300
Wire Wire Line
	4750 1600 4900 1600
Wire Wire Line
	2750 5500 2800 5500
Wire Wire Line
	2750 5400 2800 5400
Wire Wire Line
	2750 5300 2800 5300
Wire Wire Line
	2750 5200 2800 5200
Wire Wire Line
	2750 5100 2800 5100
Wire Wire Line
	2750 5000 2800 5000
Wire Wire Line
	2750 4900 2800 4900
Wire Wire Line
	2750 4800 2800 4800
Wire Wire Line
	2750 4100 2800 4100
Wire Wire Line
	2750 4000 2800 4000
Wire Wire Line
	2750 3900 2800 3900
Wire Wire Line
	2750 3800 2800 3800
Wire Wire Line
	2750 3700 2800 3700
Wire Wire Line
	2750 3600 2800 3600
Wire Wire Line
	2750 3500 2800 3500
Wire Wire Line
	2750 3400 2800 3400
Wire Wire Line
	2700 2650 2750 2650
Wire Wire Line
	2700 2550 2750 2550
Wire Wire Line
	2700 2450 2750 2450
Wire Wire Line
	2700 2350 2750 2350
Wire Wire Line
	2700 2250 2750 2250
Wire Wire Line
	2700 2150 2750 2150
Wire Wire Line
	2700 2050 2750 2050
Wire Wire Line
	2700 1950 2750 1950
Wire Wire Line
	3800 3400 4250 3400
Wire Wire Line
	1950 6050 1950 6100
Wire Wire Line
	950  6700 1100 6700
Wire Wire Line
	950  6600 1100 6600
Wire Wire Line
	950  6500 1100 6500
Wire Wire Line
	950  6400 1100 6400
Wire Wire Line
	950  6300 1100 6300
Wire Wire Line
	950  6200 1100 6200
Wire Wire Line
	2650 7050 2650 7100
Wire Wire Line
	2550 7050 2650 7050
Wire Wire Line
	2550 6950 2650 6950
Wire Wire Line
	2550 6850 2650 6850
Wire Wire Line
	2550 6750 2650 6750
Wire Wire Line
	2050 7050 1950 7050
Wire Wire Line
	2050 6950 1950 6950
Wire Wire Line
	2050 6850 1950 6850
Wire Wire Line
	1950 6750 1950 6700
Wire Wire Line
	2050 6750 1950 6750
Wire Wire Line
	2250 1300 2250 1350
Wire Wire Line
	2250 850  2250 900 
Wire Wire Line
	1950 1300 1950 1350
Wire Wire Line
	1950 850  1950 900 
Wire Wire Line
	1650 1300 1650 1350
Wire Wire Line
	1650 850  1650 900 
Wire Wire Line
	9500 2800 9600 2800
Wire Wire Line
	9050 3850 9200 3850
Wire Wire Line
	9050 3400 9050 3850
Wire Wire Line
	8950 3400 9050 3400
Wire Wire Line
	9050 3300 8950 3300
Wire Wire Line
	9050 3150 9050 3300
Wire Wire Line
	9200 3150 9050 3150
Wire Wire Line
	7000 6100 7200 6100
Wire Wire Line
	7000 4850 7000 6100
Wire Wire Line
	6950 4850 7000 4850
Wire Wire Line
	7050 5400 7200 5400
Wire Wire Line
	7050 4750 7050 5400
Wire Wire Line
	6950 4750 7050 4750
Wire Wire Line
	7050 4650 7050 4700
Wire Wire Line
	7050 4700 7200 4700
Wire Wire Line
	7050 4650 6950 4650
Wire Wire Line
	7000 4000 7200 4000
Wire Wire Line
	7000 4550 7000 4000
Wire Wire Line
	6950 4550 7000 4550
Wire Wire Line
	7000 2050 6950 2050
Wire Wire Line
	7000 3300 7000 2050
Wire Wire Line
	7200 3300 7000 3300
Wire Wire Line
	7050 2600 7050 1950
Wire Wire Line
	7200 2600 7050 2600
Wire Wire Line
	7000 1200 7200 1200
Wire Wire Line
	7000 1750 7000 1200
Wire Wire Line
	6950 1750 7000 1750
Wire Wire Line
	7050 1950 6950 1950
Wire Wire Line
	7050 1900 7200 1900
Wire Wire Line
	7050 1850 7050 1900
Wire Wire Line
	6950 1850 7050 1850
Wire Wire Line
	9600 3300 9700 3300
Wire Wire Line
	9600 3400 9700 3400
Wire Wire Line
	9600 3500 9600 3400
Wire Wire Line
	9600 2800 9600 3300
Wire Wire Line
	7650 2050 7650 2950
Wire Wire Line
	7700 2050 7650 2050
Wire Wire Line
	7600 2250 7500 2250
Wire Wire Line
	7600 1950 7600 2250
Wire Wire Line
	7700 1950 7600 1950
Wire Wire Line
	7600 1850 7600 1550
Wire Wire Line
	7700 1850 7600 1850
Wire Wire Line
	7650 1750 7650 850 
Wire Wire Line
	7700 1750 7650 1750
Wire Wire Line
	7650 4850 7700 4850
Wire Wire Line
	7650 5750 7650 4850
Wire Wire Line
	7600 4750 7700 4750
Wire Wire Line
	7600 5050 7600 4750
Wire Wire Line
	7600 4650 7700 4650
Wire Wire Line
	7600 4350 7600 4650
Wire Wire Line
	7650 4550 7700 4550
Wire Wire Line
	7650 3650 7650 4550
Wire Wire Line
	9500 4000 9500 4050
Wire Wire Line
	9500 3550 9500 3500
Wire Wire Line
	9500 3500 9600 3500
Wire Wire Line
	9500 3300 9500 3350
Wire Wire Line
	9500 2850 9500 2800
Wire Wire Line
	7500 6250 7500 6300
Wire Wire Line
	7500 5800 7500 5750
Wire Wire Line
	7500 5750 7650 5750
Wire Wire Line
	7500 5550 7500 5600
Wire Wire Line
	7500 5100 7500 5050
Wire Wire Line
	7500 5050 7600 5050
Wire Wire Line
	7500 4850 7500 4900
Wire Wire Line
	7500 4400 7500 4350
Wire Wire Line
	7500 4350 7600 4350
Wire Wire Line
	7500 4150 7500 4200
Wire Wire Line
	7500 3700 7500 3650
Wire Wire Line
	7500 3650 7650 3650
Wire Wire Line
	7500 3450 7500 3500
Wire Wire Line
	7500 3000 7500 2950
Wire Wire Line
	7500 2950 7650 2950
Wire Wire Line
	7500 2750 7500 2800
Wire Wire Line
	7500 2250 7500 2300
Wire Wire Line
	7500 2050 7500 2100
Wire Wire Line
	7500 1600 7500 1550
Wire Wire Line
	7500 1550 7600 1550
Wire Wire Line
	5200 7050 5200 7000
Wire Wire Line
	5200 6350 5200 6300
Wire Wire Line
	5200 5650 5200 5600
Wire Wire Line
	5200 4950 5200 4900
Wire Wire Line
	5200 4250 5200 4200
Wire Wire Line
	5200 3550 5200 3500
Wire Wire Line
	5200 2850 5200 2800
Wire Wire Line
	5200 2150 5200 2100
Wire Wire Line
	5200 750  5200 700 
Wire Wire Line
	1300 1650 800  1650
Wire Wire Line
	1350 3100 800  3100
Wire Wire Line
	1350 4500 800  4500
Wire Wire Line
	800  1750 1300 1750
Wire Wire Line
	1100 1750 1100 4600
Wire Wire Line
	1100 3200 1350 3200
Wire Wire Line
	1100 4600 1350 4600
Connection ~ 1100 3200
Wire Wire Line
	800  2000 1300 2000
Wire Wire Line
	1000 2000 1000 4850
Wire Wire Line
	1000 3450 1350 3450
Wire Wire Line
	1000 4850 1350 4850
Connection ~ 1000 3450
Wire Wire Line
	800  2100 1300 2100
Wire Wire Line
	900  2100 900  4950
Wire Wire Line
	900  3550 1350 3550
Wire Wire Line
	900  4950 1350 4950
Connection ~ 900  3550
Connection ~ 1100 1750
Connection ~ 1000 2000
Connection ~ 900  2100
Wire Wire Line
	5200 1450 5200 1400
Wire Wire Line
	7500 1350 7500 1400
Wire Wire Line
	7500 900  7500 850 
Wire Wire Line
	7500 850  7650 850 
Wire Wire Line
	8100 1750 8150 1750
Wire Wire Line
	3800 1300 4250 1300
Wire Wire Line
	3800 1400 4250 1400
Wire Wire Line
	3800 3200 4250 3200
Wire Wire Line
	3800 3300 4250 3300
Wire Wire Line
	3800 3500 4250 3500
Wire Wire Line
	3800 6000 4250 6000
Wire Wire Line
	3800 6100 4250 6100
Wire Wire Line
	6100 1750 6550 1750
Wire Wire Line
	8100 1850 8150 1850
Wire Wire Line
	6100 1850 6550 1850
Wire Wire Line
	8100 1950 8150 1950
Wire Wire Line
	6100 1950 6550 1950
Wire Wire Line
	8100 2050 8150 2050
Wire Wire Line
	6100 2050 6550 2050
Wire Wire Line
	8100 4550 8150 4550
Wire Wire Line
	6100 4550 6550 4550
Wire Wire Line
	8100 4650 8150 4650
Wire Wire Line
	6100 4650 6550 4650
Wire Wire Line
	8100 4750 8150 4750
Wire Wire Line
	6100 4750 6550 4750
Wire Wire Line
	8100 4850 8150 4850
Wire Wire Line
	6100 4850 6550 4850
Wire Wire Line
	10100 3300 10150 3300
Wire Wire Line
	8100 3300 8550 3300
Wire Wire Line
	10100 3400 10150 3400
Wire Wire Line
	8100 3400 8550 3400
Wire Wire Line
	9150 5100 9200 5100
Wire Wire Line
	9200 5200 9150 5200
Wire Wire Line
	9150 5300 9200 5300
Wire Wire Line
	9200 5400 9150 5400
Wire Wire Line
	9150 5500 9200 5500
Wire Wire Line
	9200 5600 9150 5600
Wire Wire Line
	9150 5700 9200 5700
Wire Wire Line
	9200 5800 9150 5800
Wire Wire Line
	9150 5900 9200 5900
Wire Wire Line
	9200 6000 9150 6000
Wire Wire Line
	9700 6000 9750 6000
Wire Wire Line
	9750 5900 9700 5900
Wire Wire Line
	9700 5800 9750 5800
Wire Wire Line
	9750 5700 9700 5700
Wire Wire Line
	9700 5600 9750 5600
Wire Wire Line
	9750 5500 9700 5500
Wire Wire Line
	9700 5400 9750 5400
Wire Wire Line
	9750 5300 9700 5300
Wire Wire Line
	9700 5200 9750 5200
Wire Wire Line
	9750 5100 9700 5100
Wire Wire Line
	5200 1200 5300 1200
Wire Wire Line
	5200 1900 5300 1900
Wire Wire Line
	5200 2600 5300 2600
Wire Wire Line
	5200 3300 5300 3300
Wire Wire Line
	5200 4000 5300 4000
Wire Wire Line
	5200 4700 5300 4700
Wire Wire Line
	5200 5400 5300 5400
Wire Wire Line
	5200 6100 5300 6100
Wire Wire Line
	5200 6800 5300 6800
Wire Wire Line
	5200 7500 5300 7500
Wire Wire Line
	3900 1100 4250 1100
Wire Wire Line
	4000 1200 4250 1200
Wire Wire Line
	4750 6500 4750 6200
Wire Wire Line
	4750 6100 4750 5800
Wire Wire Line
	4750 5800 4900 5800
Wire Wire Line
	4700 6000 4700 5100
Wire Wire Line
	4700 6300 4700 7200
Wire Wire Line
	4750 3700 4900 3700
Wire Wire Line
	4650 6000 4700 6000
Wire Wire Line
	4650 6100 4750 6100
Wire Wire Line
	4750 6200 4650 6200
Wire Wire Line
	4650 6300 4700 6300
Wire Wire Line
	4900 4400 4700 4400
Wire Wire Line
	4700 4400 4700 3500
Wire Wire Line
	4700 3500 4650 3500
Wire Wire Line
	4650 3400 4750 3400
Wire Wire Line
	4750 3400 4750 3700
Wire Wire Line
	4650 3300 4750 3300
Wire Wire Line
	4750 3300 4750 3000
Wire Wire Line
	4750 3000 4900 3000
Wire Wire Line
	4650 3200 4700 3200
Wire Wire Line
	4700 3200 4700 2300
Wire Wire Line
	4650 1300 4750 1300
Wire Wire Line
	4750 1300 4750 900 
Wire Wire Line
	4750 900  4900 900 
Wire Wire Line
	4750 1400 4750 1600
NoConn ~ 4650 1200
NoConn ~ 4650 1100
$EndSCHEMATC
