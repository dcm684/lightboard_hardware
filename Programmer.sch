EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R9
U 1 1 54E63D1D
P 6550 2250
F 0 "R9" V 6630 2250 40  0000 C CNN
F 1 "10k" V 6557 2251 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6480 2250 30  0001 C CNN
F 3 "" H 6550 2250 30  0000 C CNN
	1    6550 2250
	-1   0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 54E63D2B
P 6550 2800
F 0 "C10" H 6550 2900 40  0000 L CNN
F 1 "0uF" H 6556 2715 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6588 2650 30  0001 C CNN
F 3 "" H 6550 2800 60  0000 C CNN
	1    6550 2800
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR058
U 1 1 54E63D39
P 6550 3050
F 0 "#PWR058" H 6550 3050 30  0001 C CNN
F 1 "GND" H 6550 2980 30  0001 C CNN
F 2 "" H 6550 3050 60  0000 C CNN
F 3 "" H 6550 3050 60  0000 C CNN
	1    6550 3050
	-1   0    0    -1  
$EndComp
$Comp
L VCC #PWR059
U 1 1 54E63D3F
P 6550 1900
F 0 "#PWR059" H 6550 2000 30  0001 C CNN
F 1 "VCC" H 6550 2000 30  0000 C CNN
F 2 "" H 6550 1900 60  0000 C CNN
F 3 "" H 6550 1900 60  0000 C CNN
	1    6550 1900
	-1   0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 54E63D46
P 7550 2550
F 0 "SW1" H 7700 2660 50  0000 C CNN
F 1 "SW_PUSH" H 7550 2470 50  0000 C CNN
F 2 "Personal_SMD:Tyco-FSMSMTR" H 7550 2550 60  0001 C CNN
F 3 "" H 7550 2550 60  0000 C CNN
	1    7550 2550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR060
U 1 1 54E63D4E
P 7850 3050
F 0 "#PWR060" H 7850 3050 30  0001 C CNN
F 1 "GND" H 7850 2980 30  0001 C CNN
F 2 "" H 7850 3050 60  0000 C CNN
F 3 "" H 7850 3050 60  0000 C CNN
	1    7850 3050
	-1   0    0    -1  
$EndComp
Text Label 6000 2550 2    60   ~ 0
Reset
Text HLabel 6150 3700 0    60   Output ~ 0
Reset
Text HLabel 6950 3600 2    60   Output ~ 0
MOSI
Text HLabel 6150 3500 0    60   Input ~ 0
MISO
Text HLabel 6150 3600 0    60   Output ~ 0
SCK
Text Label 6150 3850 2    60   ~ 0
Reset
$Comp
L GND #PWR061
U 1 1 54E6434D
P 7000 3750
F 0 "#PWR061" H 7000 3750 30  0001 C CNN
F 1 "GND" H 7000 3680 30  0001 C CNN
F 2 "" H 7000 3750 60  0000 C CNN
F 3 "" H 7000 3750 60  0000 C CNN
	1    7000 3750
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR062
U 1 1 54E64366
P 7000 3450
F 0 "#PWR062" H 7000 3550 30  0001 C CNN
F 1 "VCC" H 7000 3550 30  0000 C CNN
F 2 "" H 7000 3450 60  0000 C CNN
F 3 "" H 7000 3450 60  0000 C CNN
	1    7000 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2500 6550 2600
Wire Wire Line
	6200 2550 6200 2500
Connection ~ 6550 2550
Wire Wire Line
	6550 1900 6550 2000
Wire Wire Line
	6550 1950 6200 1950
Wire Wire Line
	6200 1950 6200 2100
Wire Wire Line
	6550 3000 6550 3050
Connection ~ 6550 1950
Wire Wire Line
	7850 2550 7850 3050
Connection ~ 6200 2550
Wire Wire Line
	6000 2550 6700 2550
Wire Wire Line
	6150 3850 6250 3850
Wire Wire Line
	6250 3850 6250 3700
Connection ~ 6250 3700
Wire Wire Line
	6850 3500 7000 3500
Wire Wire Line
	7000 3500 7000 3450
Wire Wire Line
	6850 3700 7000 3700
Wire Wire Line
	7000 3700 7000 3750
Wire Wire Line
	6150 3700 6350 3700
Wire Wire Line
	6150 3600 6350 3600
Wire Wire Line
	6150 3500 6350 3500
Wire Wire Line
	6950 3600 6850 3600
$Comp
L R R10
U 1 1 54E59D98
P 6950 2550
F 0 "R10" V 7030 2550 40  0000 C CNN
F 1 "330" V 6957 2551 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 2550 30  0001 C CNN
F 3 "" H 6950 2550 30  0000 C CNN
	1    6950 2550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7200 2550 7250 2550
$Comp
L ZENER D2
U 1 1 54E59FCD
P 6200 2300
F 0 "D2" H 6200 2400 50  0000 C CNN
F 1 "ZENER" H 6200 2200 40  0000 C CNN
F 2 "Diodes_SMD:Diode-SMA_Handsoldering" H 6200 2300 60  0001 C CNN
F 3 "" H 6200 2300 60  0000 C CNN
	1    6200 2300
	0    -1   -1   0   
$EndComp
$Comp
L CONN_02X03 J7
U 1 1 54E63F95
P 6600 3600
F 0 "J7" H 6600 3800 50  0000 C CNN
F 1 "CONN_02X03" H 6600 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 6600 2400 60  0001 C CNN
F 3 "" H 6600 2400 60  0000 C CNN
	1    6600 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
