EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L C C11
U 1 1 54E519E3
P 6200 3150
F 0 "C11" H 6200 3250 40  0000 L CNN
F 1 "0.1uF" H 6206 3065 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6238 3000 30  0001 C CNN
F 3 "" H 6200 3150 60  0000 C CNN
	1    6200 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6400 3350 6200 3350
Wire Wire Line
	6400 3250 6350 3250
Wire Wire Line
	6350 3250 6350 2900
Wire Wire Line
	6350 2900 6200 2900
Wire Wire Line
	6200 2900 6200 2950
$Comp
L VCC #PWR063
U 1 1 54E51A38
P 6200 2900
F 0 "#PWR063" H 6200 3000 30  0001 C CNN
F 1 "VCC" H 6200 3000 30  0000 C CNN
F 2 "" H 6200 2900 60  0000 C CNN
F 3 "" H 6200 2900 60  0000 C CNN
	1    6200 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR064
U 1 1 54E51A4C
P 6200 3400
F 0 "#PWR064" H 6200 3400 30  0001 C CNN
F 1 "GND" H 6200 3330 30  0001 C CNN
F 2 "" H 6200 3400 60  0000 C CNN
F 3 "" H 6200 3400 60  0000 C CNN
	1    6200 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3350 6200 3400
Wire Wire Line
	6400 3550 6300 3550
Wire Wire Line
	6400 3650 6300 3650
Wire Wire Line
	6400 3750 6300 3750
Wire Wire Line
	6400 3850 6300 3850
Wire Wire Line
	6400 4000 6300 4000
Wire Wire Line
	6400 4100 6300 4100
Text HLabel 6300 3550 0    60   Input ~ 0
SI
Text HLabel 6300 3650 0    60   Output ~ 0
SO
Text HLabel 6300 3750 0    60   Input ~ 0
~CS
Text HLabel 6300 3850 0    60   Input ~ 0
SCK
Text HLabel 6300 4000 0    60   Input ~ 0
~HOLD
Text HLabel 6300 4100 0    60   Input ~ 0
~WP
$Comp
L 25AA640A U5
U 1 1 54E83384
P 6500 3750
F 0 "U5" H 6500 4350 60  0000 L CNN
F 1 "25AA640A" H 6950 3250 60  0000 R CNN
F 2 "Personal_SMD:MSOP-8" H 6500 3500 60  0001 C CNN
F 3 "" H 6500 3500 60  0000 C CNN
	1    6500 3750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
