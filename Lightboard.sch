EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9100 2400 1050 700 
U 54DE5899
F0 "LED Driver" 60
F1 "LED_Driver.sch" 60
F2 "Data_0" I L 9100 2500 60 
F3 "Data_1" I L 9100 2600 60 
F4 "Data_2" I L 9100 2700 60 
F5 "Clock" I L 9100 2800 60 
F6 "Strobe" I L 9100 2900 60 
F7 "OE" I L 9100 3000 60 
$EndSheet
$Comp
L ATMEGA32U2 U1
U 1 1 54E5CE99
P 5250 3900
F 0 "U1" H 4400 5250 60  0000 C CNN
F 1 "ATMEGA32U2" H 6000 2550 50  0000 C CNN
F 2 "Personal_SMD:TQFP32-8.40mmX8.40mm-0.80mmPitch" H 5100 3950 50  0001 C CNN
F 3 "http://www.atmel.com/Images/doc7799.pdf" H 5350 2450 50  0001 C CNN
	1    5250 3900
	1    0    0    -1  
$EndComp
$Sheet
S 2300 4000 1150 650 
U 54E5E07F
F0 "USB Interface" 60
F1 "USB_Interface.sch" 60
F2 "UCAP" U R 3450 4100 60 
F3 "D+" B R 3450 4300 60 
F4 "D-" I R 3450 4400 60 
F5 "UID" I R 3450 4600 60 
F6 "UVSS" U R 3450 4500 60 
F7 "UVCC" U R 3450 4200 60 
F8 "VBUS" U L 2300 4300 60 
$EndSheet
NoConn ~ 3450 4600
$Comp
L CRYSTAL X1
U 1 1 54E6056F
P 3200 3450
F 0 "X1" H 3200 3600 60  0000 C CNN
F 1 "8MHz" H 3200 3300 60  0000 C CNN
F 2 "Personal_SMD:TXC-5x3.22mm-Crystal" H 3200 3450 60  0001 C CNN
F 3 "" H 3200 3450 60  0000 C CNN
	1    3200 3450
	0    1    1    0   
$EndComp
$Comp
L C C2
U 1 1 54E608DB
P 3650 3750
F 0 "C2" H 3650 3850 40  0000 L CNN
F 1 "30pF" H 3656 3665 40  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3688 3600 30  0001 C CNN
F 3 "" H 3650 3750 60  0000 C CNN
	1    3650 3750
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 54E60918
P 3850 3750
F 0 "C3" H 3850 3850 40  0000 L CNN
F 1 "30pF" H 3856 3665 40  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3888 3600 30  0001 C CNN
F 3 "" H 3850 3750 60  0000 C CNN
	1    3850 3750
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR01
U 1 1 54E60FE2
P 3750 2700
F 0 "#PWR01" H 3750 2800 30  0001 C CNN
F 1 "VCC" H 3750 2800 30  0000 C CNN
F 2 "" H 3750 2700 60  0000 C CNN
F 3 "" H 3750 2700 60  0000 C CNN
	1    3750 2700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 54E60963
P 3650 4000
F 0 "#PWR02" H 3650 4000 30  0001 C CNN
F 1 "GND" H 3650 3930 30  0001 C CNN
F 2 "" H 3650 4000 60  0000 C CNN
F 3 "" H 3650 4000 60  0000 C CNN
	1    3650 4000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 54E6094F
P 3850 4000
F 0 "#PWR03" H 3850 4000 30  0001 C CNN
F 1 "GND" H 3850 3930 30  0001 C CNN
F 2 "" H 3850 4000 60  0000 C CNN
F 3 "" H 3850 4000 60  0000 C CNN
	1    3850 4000
	1    0    0    -1  
$EndComp
Text Label 4100 3200 2    60   ~ 0
RESET
$Sheet
S 9100 800  1050 500 
U 54E63730
F0 "Reset and Programmer" 60
F1 "Programmer.sch" 60
F2 "Reset" O L 9100 900 60 
F3 "MOSI" O L 9100 1000 60 
F4 "MISO" I L 9100 1100 60 
F5 "SCK" O L 9100 1200 60 
$EndSheet
Text Label 9000 900  2    60   ~ 0
RESET
Text Label 6650 2900 0    60   ~ 0
SCK_RAW
Text Label 6650 3000 0    60   ~ 0
MOSI_RAW
Text Label 6650 3100 0    60   ~ 0
MISO_RAW
Text Label 9000 1200 2    60   ~ 0
SCK_RAW
Text Label 9000 1000 2    60   ~ 0
MOSI_RAW
Text Label 9000 1100 2    60   ~ 0
MISO_RAW
$Comp
L R R2
U 1 1 54E6737E
P 6950 2150
F 0 "R2" V 7030 2150 40  0000 C CNN
F 1 "4.7k" V 6957 2151 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 2150 30  0001 C CNN
F 3 "" H 6950 2150 30  0000 C CNN
	1    6950 2150
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 54E673C5
P 6950 2300
F 0 "R3" V 7030 2300 40  0000 C CNN
F 1 "4.7k" V 6957 2301 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 2300 30  0001 C CNN
F 3 "" H 6950 2300 30  0000 C CNN
	1    6950 2300
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 54E673F4
P 6950 2450
F 0 "R4" V 7030 2450 40  0000 C CNN
F 1 "4.7k" V 6957 2451 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 2450 30  0001 C CNN
F 3 "" H 6950 2450 30  0000 C CNN
	1    6950 2450
	0    1    1    0   
$EndComp
Text Label 7250 2150 0    60   ~ 0
SCK_SPI
Text Label 7250 2300 0    60   ~ 0
MOSI_SPI
Text Label 7250 2450 0    60   ~ 0
MISO_SPI
Text Label 6450 3900 0    60   ~ 0
UART_RX
Text Label 6450 4000 0    60   ~ 0
UART_TX
Text Label 6450 3700 0    60   ~ 0
LED_STROBE
Text Label 6450 3800 0    60   ~ 0
LED_OE
Text Label 6450 4100 0    60   ~ 0
LED_DATA_0
Text Label 6450 4200 0    60   ~ 0
LED_DATA_1
Text Label 6450 4300 0    60   ~ 0
LED_DATA_2
Text Label 6450 4400 0    60   ~ 0
LED_CLOCK
Text Label 9000 2500 2    60   ~ 0
LED_DATA_0
Text Label 9000 2600 2    60   ~ 0
LED_DATA_1
Text Label 9000 2700 2    60   ~ 0
LED_DATA_2
Text Label 9000 2800 2    60   ~ 0
LED_CLOCK
Text Label 9000 2900 2    60   ~ 0
LED_STROBE
Text Label 9000 3000 2    60   ~ 0
LED_OE
$Sheet
S 9100 3550 1050 700 
U 54E5176A
F0 "EEPROM" 60
F1 "EEPROM.sch" 60
F2 "SI" I L 9100 3650 60 
F3 "SO" O L 9100 3750 60 
F4 "~CS" I L 9100 3950 60 
F5 "SCK" I L 9100 3850 60 
F6 "~HOLD" I L 9100 4050 60 
F7 "~WP" I L 9100 4150 60 
$EndSheet
Text Label 9000 3650 2    60   ~ 0
MOSI_SPI
Text Label 9000 3750 2    60   ~ 0
MISO_SPI
Text Label 9000 3950 2    60   ~ 0
EEPROM_CS
Text Label 9000 3850 2    60   ~ 0
SCK_SPI
Text Label 9000 4050 2    60   ~ 0
EEPROM_HOLD
Text Label 9000 4150 2    60   ~ 0
EEPROM_WP
Text Label 6450 3200 0    60   ~ 0
EEPROM_CS
Text Label 6450 3300 0    60   ~ 0
EEPROM_HOLD
Text Label 6450 3400 0    60   ~ 0
EEPROM_WP
$Comp
L C C4
U 1 1 54E54394
P 5000 2300
F 0 "C4" H 5000 2400 40  0000 L CNN
F 1 "0.1uF" H 5006 2215 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5038 2150 30  0001 C CNN
F 3 "" H 5000 2300 60  0000 C CNN
	1    5000 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 54E54400
P 5250 5350
F 0 "#PWR04" H 5250 5350 30  0001 C CNN
F 1 "GND" H 5250 5280 30  0001 C CNN
F 2 "" H 5250 5350 60  0000 C CNN
F 3 "" H 5250 5350 60  0000 C CNN
	1    5250 5350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 54E54413
P 5000 2550
F 0 "#PWR05" H 5000 2550 30  0001 C CNN
F 1 "GND" H 5000 2480 30  0001 C CNN
F 2 "" H 5000 2550 60  0000 C CNN
F 3 "" H 5000 2550 60  0000 C CNN
	1    5000 2550
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR06
U 1 1 54E5473E
P 5250 1500
F 0 "#PWR06" H 5250 1600 30  0001 C CNN
F 1 "VCC" H 5250 1600 30  0000 C CNN
F 2 "" H 5250 1500 60  0000 C CNN
F 3 "" H 5250 1500 60  0000 C CNN
	1    5250 1500
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 54E55DF3
P 9350 4550
F 0 "R5" V 9430 4550 40  0000 C CNN
F 1 "1k" V 9357 4551 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 9280 4550 30  0001 C CNN
F 3 "" H 9350 4550 30  0000 C CNN
	1    9350 4550
	0    1    1    0   
$EndComp
$Comp
L LED D1
U 1 1 54E55ED9
P 9900 4550
F 0 "D1" H 9900 4650 50  0000 C CNN
F 1 "LED" H 9900 4450 50  0000 C CNN
F 2 "Custom_SMD:LED-1206_No_pad_Silk" H 9900 4550 60  0001 C CNN
F 3 "" H 9900 4550 60  0000 C CNN
	1    9900 4550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR07
U 1 1 54E56215
P 10200 4600
F 0 "#PWR07" H 10200 4600 30  0001 C CNN
F 1 "GND" H 10200 4530 30  0001 C CNN
F 2 "" H 10200 4600 60  0000 C CNN
F 3 "" H 10200 4600 60  0000 C CNN
	1    10200 4600
	1    0    0    -1  
$EndComp
Text Label 9000 4550 2    60   ~ 0
HEART
Text Label 6450 4600 0    60   ~ 0
HEART
$Sheet
S 900  750  1050 500 
U 54E57B3C
F0 "Power Supply" 60
F1 "Power_Supply.sch" 60
F2 "+5V" O R 1950 850 60 
F3 "GND" O R 1950 950 60 
F4 "USB_VCC" I R 1950 1150 60 
$EndSheet
$Comp
L Ferrite_Bead FB1
U 1 1 54E5819B
P 5250 1800
F 0 "FB1" V 5100 1700 60  0000 C CNN
F 1 "Ferrite_Bead" H 5250 1950 60  0001 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5350 1800 60  0001 C CNN
F 3 "" H 5350 1800 60  0000 C CNN
	1    5250 1800
	0    1    1    0   
$EndComp
$Comp
L C C1
U 1 1 54E59117
P 3600 3000
F 0 "C1" H 3600 3100 40  0000 L CNN
F 1 "0.1uF" H 3606 2915 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3638 2850 30  0001 C CNN
F 3 "" H 3600 3000 60  0000 C CNN
	1    3600 3000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 54E5921E
P 3600 3250
F 0 "#PWR08" H 3600 3250 30  0001 C CNN
F 1 "GND" H 3600 3180 30  0001 C CNN
F 2 "" H 3600 3250 60  0000 C CNN
F 3 "" H 3600 3250 60  0000 C CNN
	1    3600 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 54E64B0A
P 2050 1000
F 0 "#PWR09" H 2050 1000 30  0001 C CNN
F 1 "GND" H 2050 930 30  0001 C CNN
F 2 "" H 2050 1000 60  0000 C CNN
F 3 "" H 2050 1000 60  0000 C CNN
	1    2050 1000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR010
U 1 1 54E64FFF
P 2200 800
F 0 "#PWR010" H 2200 900 30  0001 C CNN
F 1 "VCC" H 2200 900 30  0000 C CNN
F 2 "" H 2200 800 60  0000 C CNN
F 3 "" H 2200 800 60  0000 C CNN
	1    2200 800 
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 J2
U 1 1 54E66049
P 9300 5150
F 0 "J2" H 9300 5500 50  0000 C CNN
F 1 "Serial Interface" V 9400 5150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 9300 5150 60  0001 C CNN
F 3 "" H 9300 5150 60  0000 C CNN
	1    9300 5150
	1    0    0    -1  
$EndComp
NoConn ~ 9100 4900
NoConn ~ 9100 5200
NoConn ~ 9100 5300
$Comp
L GND #PWR011
U 1 1 54E66B3D
P 9000 5450
F 0 "#PWR011" H 9000 5450 30  0001 C CNN
F 1 "GND" H 9000 5380 30  0001 C CNN
F 2 "" H 9000 5450 60  0000 C CNN
F 3 "" H 9000 5450 60  0000 C CNN
	1    9000 5450
	1    0    0    -1  
$EndComp
Text Label 9000 5000 2    60   ~ 0
UART_TX
Text Label 9000 5100 2    60   ~ 0
UART_RX
Text Label 6450 3500 0    60   ~ 0
AUX_SPI_CS
$Comp
L CONN_01X06 J1
U 1 1 54E686A4
P 9300 1900
F 0 "J1" H 9300 2250 50  0000 C CNN
F 1 "Aux SPI Interface" V 9400 1900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 9300 1900 60  0001 C CNN
F 3 "" H 9300 1900 60  0000 C CNN
	1    9300 1900
	1    0    0    -1  
$EndComp
Text Label 9000 1750 2    60   ~ 0
MOSI_SPI
Text Label 9000 1850 2    60   ~ 0
MISO_SPI
Text Label 9000 1950 2    60   ~ 0
SCK_SPI
Text Label 9000 2050 2    60   ~ 0
AUX_SPI_CS
$Comp
L VCC #PWR012
U 1 1 54E68C95
P 9000 1600
F 0 "#PWR012" H 9000 1700 30  0001 C CNN
F 1 "VCC" H 9000 1700 30  0000 C CNN
F 2 "" H 9000 1600 60  0000 C CNN
F 3 "" H 9000 1600 60  0000 C CNN
	1    9000 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 54E68E0D
P 9000 2200
F 0 "#PWR013" H 9000 2200 30  0001 C CNN
F 1 "GND" H 9000 2130 30  0001 C CNN
F 2 "" H 9000 2200 60  0000 C CNN
F 3 "" H 9000 2200 60  0000 C CNN
	1    9000 2200
	1    0    0    -1  
$EndComp
Text Label 6450 4700 0    60   ~ 0
AUX_CONN_PC4
Text Label 6450 4800 0    60   ~ 0
AUX_CONN_PC5
Text Label 6450 4900 0    60   ~ 0
AUX_CONN_PC6
Wire Wire Line
	3650 3950 3650 4000
Wire Wire Line
	3850 3950 3850 4000
Wire Wire Line
	3750 2700 3750 3300
Wire Wire Line
	3750 3300 4150 3300
Connection ~ 3650 3500
Wire Wire Line
	3650 3550 3650 3500
Connection ~ 3850 3400
Wire Wire Line
	3850 3550 3850 3400
Wire Wire Line
	3200 3100 3200 3150
Wire Wire Line
	3400 3100 3200 3100
Wire Wire Line
	3400 3400 3400 3100
Wire Wire Line
	3400 3400 4150 3400
Wire Wire Line
	3200 3800 3200 3750
Wire Wire Line
	3400 3800 3200 3800
Wire Wire Line
	3400 3500 3400 3800
Wire Wire Line
	3400 3500 4150 3500
Wire Wire Line
	3450 4500 4150 4500
Wire Wire Line
	3450 4400 4150 4400
Wire Wire Line
	4150 4300 3450 4300
Wire Wire Line
	3450 4200 4150 4200
Wire Wire Line
	4150 4100 3450 4100
Wire Wire Line
	4150 3200 4100 3200
Wire Wire Line
	9100 900  9000 900 
Wire Wire Line
	6350 2900 6650 2900
Wire Wire Line
	6350 3000 6650 3000
Wire Wire Line
	6350 3100 6650 3100
Wire Wire Line
	9100 1200 9000 1200
Wire Wire Line
	9100 1000 9000 1000
Wire Wire Line
	9100 1100 9000 1100
Wire Wire Line
	6500 2900 6500 2150
Wire Wire Line
	6500 2150 6700 2150
Connection ~ 6500 2900
Wire Wire Line
	6550 3000 6550 2300
Wire Wire Line
	6550 2300 6700 2300
Connection ~ 6550 3000
Wire Wire Line
	6600 3100 6600 2450
Wire Wire Line
	6600 2450 6700 2450
Connection ~ 6600 3100
Wire Wire Line
	7200 2150 7250 2150
Wire Wire Line
	7200 2300 7250 2300
Wire Wire Line
	7200 2450 7250 2450
Wire Wire Line
	6350 3700 6450 3700
Wire Wire Line
	6350 3800 6450 3800
Wire Wire Line
	6350 3900 6450 3900
Wire Wire Line
	6350 4000 6450 4000
Wire Wire Line
	6350 4100 6450 4100
Wire Wire Line
	6350 4200 6450 4200
Wire Wire Line
	6350 4300 6450 4300
Wire Wire Line
	6350 4400 6450 4400
Wire Wire Line
	9100 2500 9000 2500
Wire Wire Line
	9100 2600 9000 2600
Wire Wire Line
	9100 2700 9000 2700
Wire Wire Line
	9100 2800 9000 2800
Wire Wire Line
	9100 2900 9000 2900
Wire Wire Line
	9100 3000 9000 3000
Wire Wire Line
	9100 3650 9000 3650
Wire Wire Line
	9100 3750 9000 3750
Wire Wire Line
	9100 3850 9000 3850
Wire Wire Line
	9100 3950 9000 3950
Wire Wire Line
	9100 4050 9000 4050
Wire Wire Line
	9100 4150 9000 4150
Wire Wire Line
	6350 3200 6450 3200
Wire Wire Line
	6350 3300 6450 3300
Wire Wire Line
	6350 3400 6450 3400
Wire Wire Line
	6350 3500 6450 3500
Wire Wire Line
	5000 2500 5000 2550
Wire Wire Line
	5000 2100 5000 2050
Wire Wire Line
	5000 2050 5500 2050
Wire Wire Line
	5250 2050 5250 2500
Wire Wire Line
	5250 5300 5250 5350
Wire Wire Line
	9600 4550 9700 4550
Wire Wire Line
	10100 4550 10200 4550
Wire Wire Line
	10200 4550 10200 4600
Wire Wire Line
	9100 4550 9000 4550
Wire Wire Line
	6350 4600 6450 4600
Wire Wire Line
	5250 1550 5250 1500
Wire Wire Line
	3600 2800 3600 2750
Wire Wire Line
	3600 3200 3600 3250
Wire Wire Line
	3600 2750 3750 2750
Connection ~ 3750 2750
Wire Wire Line
	1950 950  2050 950 
Wire Wire Line
	2050 950  2050 1000
Wire Wire Line
	1950 850  2200 850 
Wire Wire Line
	2050 850  2050 800 
Wire Wire Line
	9100 5100 9000 5100
Wire Wire Line
	9100 5000 9000 5000
Wire Wire Line
	9100 1650 9000 1650
Wire Wire Line
	9100 1750 9000 1750
Wire Wire Line
	9100 1850 9000 1850
Wire Wire Line
	9100 1950 9000 1950
Wire Wire Line
	9100 2050 9000 2050
Wire Wire Line
	9100 2150 9000 2150
Wire Wire Line
	9000 2150 9000 2200
Wire Wire Line
	9000 1650 9000 1600
Wire Wire Line
	6350 5000 6450 5000
Wire Wire Line
	6350 4700 6450 4700
Wire Wire Line
	6350 4800 6450 4800
Wire Wire Line
	6350 4900 6450 4900
Wire Wire Line
	9100 5750 9000 5750
Wire Wire Line
	9100 5950 9000 5950
Wire Wire Line
	9100 6050 9000 6050
Wire Wire Line
	9100 6150 9000 6150
Wire Wire Line
	9100 6250 9000 6250
Wire Wire Line
	9000 5750 9000 5700
Wire Wire Line
	9000 6250 9000 6300
Text Label 9000 5950 2    60   ~ 0
AUX_CONN_PC4
Text Label 9000 6050 2    60   ~ 0
AUX_CONN_PC5
Text Label 9000 6150 2    60   ~ 0
AUX_CONN_PC6
$Comp
L VCC #PWR014
U 1 1 54E73F97
P 9000 5700
F 0 "#PWR014" H 9000 5800 30  0001 C CNN
F 1 "VCC" H 9000 5800 30  0000 C CNN
F 2 "" H 9000 5700 60  0000 C CNN
F 3 "" H 9000 5700 60  0000 C CNN
	1    9000 5700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 54E73FAB
P 9000 6300
F 0 "#PWR015" H 9000 6300 30  0001 C CNN
F 1 "GND" H 9000 6230 30  0001 C CNN
F 2 "" H 9000 6300 60  0000 C CNN
F 3 "" H 9000 6300 60  0000 C CNN
	1    9000 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5400 9000 5400
Wire Wire Line
	9000 5400 9000 5450
Wire Wire Line
	6350 2800 6650 2800
Text Label 6650 2800 0    60   ~ 0
AUX_CONN_PB0
Text Label 9000 5850 2    60   ~ 0
AUX_CONN_PB0
Wire Wire Line
	9100 5850 9000 5850
$Comp
L +5V #PWR016
U 1 1 54E7B755
P 2050 800
F 0 "#PWR016" H 2050 890 20  0001 C CNN
F 1 "+5V" H 2050 890 30  0000 C CNN
F 2 "" H 2050 800 60  0000 C CNN
F 3 "" H 2050 800 60  0000 C CNN
	1    2050 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 850  2200 800 
Connection ~ 2050 850 
$Comp
L PWR_FLAG #FLG017
U 1 1 54E7C88F
P 5500 2050
F 0 "#FLG017" H 5500 2145 30  0001 C CNN
F 1 "PWR_FLAG" H 5500 2230 30  0000 C CNN
F 2 "" H 5500 2050 60  0000 C CNN
F 3 "" H 5500 2050 60  0000 C CNN
	1    5500 2050
	1    0    0    -1  
$EndComp
Connection ~ 5250 2050
$Comp
L CONN_01X06 J3
U 1 1 54E7E22D
P 9300 6000
F 0 "J3" H 9300 6350 50  0000 C CNN
F 1 "Unconnected Pins" V 9400 6000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 9300 6000 60  0001 C CNN
F 3 "" H 9300 6000 60  0000 C CNN
	1    9300 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4300 2200 4300
Text Label 2200 4300 2    60   ~ 0
USB_VBUS
Text Label 2050 1150 0    60   ~ 0
USB_VBUS
Wire Wire Line
	1950 1150 2050 1150
Text Label 7000 5000 0    60   ~ 0
USB_VBUS
$Comp
L R R1
U 1 1 54E952F7
P 6700 5000
F 0 "R1" V 6780 5000 40  0000 C CNN
F 1 "4.7k" V 6707 5001 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6630 5000 30  0001 C CNN
F 3 "" H 6700 5000 30  0000 C CNN
	1    6700 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 5000 7000 5000
$EndSCHEMATC
