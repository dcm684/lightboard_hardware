EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TPS562209DDC U6
U 1 1 54E60C70
P 5450 4000
F 0 "U6" H 5200 4225 60  0000 L CNN
F 1 "TPS562209DDC" H 5450 3750 60  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT23-6" H 5200 3900 60  0001 C CNN
F 3 "" H 5200 3900 60  0000 C CNN
	1    5450 4000
	1    0    0    -1  
$EndComp
$Comp
L BARREL_JACK J8
U 1 1 54E60F73
P 3550 4000
F 0 "J8" H 3550 4250 60  0000 C CNN
F 1 "BARREL_JACK" H 3550 3800 60  0000 C CNN
F 2 "Personal_Connectors:MPD-EJ508A-Barrel_Jack" H 3550 4000 60  0001 C CNN
F 3 "" H 3550 4000 60  0000 C CNN
	1    3550 4000
	1    0    0    -1  
$EndComp
$Comp
L C C12
U 1 1 54E61048
P 4350 4150
F 0 "C12" H 4350 4250 40  0000 L CNN
F 1 "22uF" H 4356 4065 40  0000 L CNN
F 2 "Capacitors_SMD:C_1210_HandSoldering" H 4388 4000 30  0001 C CNN
F 3 "" H 4350 4150 60  0000 C CNN
	1    4350 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR065
U 1 1 54E61132
P 4900 4400
F 0 "#PWR065" H 4900 4400 30  0001 C CNN
F 1 "GND" H 4900 4330 30  0001 C CNN
F 2 "" H 4900 4400 60  0000 C CNN
F 3 "" H 4900 4400 60  0000 C CNN
	1    4900 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR066
U 1 1 54E61147
P 4350 4400
F 0 "#PWR066" H 4350 4400 30  0001 C CNN
F 1 "GND" H 4350 4330 30  0001 C CNN
F 2 "" H 4350 4400 60  0000 C CNN
F 3 "" H 4350 4400 60  0000 C CNN
	1    4350 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR067
U 1 1 54E61155
P 3950 4400
F 0 "#PWR067" H 3950 4400 30  0001 C CNN
F 1 "GND" H 3950 4330 30  0001 C CNN
F 2 "" H 3950 4400 60  0000 C CNN
F 3 "" H 3950 4400 60  0000 C CNN
	1    3950 4400
	1    0    0    -1  
$EndComp
$Comp
L C C14
U 1 1 54E61706
P 6150 4000
F 0 "C14" H 6150 4100 40  0000 L CNN
F 1 "0.1uF" H 6156 3915 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6188 3850 30  0001 C CNN
F 3 "" H 6150 4000 60  0000 C CNN
	1    6150 4000
	0    1    1    0   
$EndComp
$Comp
L INDUCTOR L1
U 1 1 54E61764
P 6750 3900
F 0 "L1" V 6700 3900 40  0000 C CNN
F 1 "4.70uH" V 6850 3900 40  0000 C CNN
F 2 "Personal_SMD:BOURNS-Inductor-6.0mmX6.0mm" H 6750 3900 60  0001 C CNN
F 3 "" H 6750 3900 60  0000 C CNN
	1    6750 3900
	0    -1   -1   0   
$EndComp
$Comp
L R R15
U 1 1 54E618B3
P 7100 4200
F 0 "R15" V 7180 4200 40  0000 C CNN
F 1 "54.9K" V 7107 4201 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7030 4200 30  0001 C CNN
F 3 "" H 7100 4200 30  0000 C CNN
	1    7100 4200
	1    0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 54E61916
P 7100 4800
F 0 "R16" V 7180 4800 40  0000 C CNN
F 1 "10.0K" V 7107 4801 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7030 4800 30  0001 C CNN
F 3 "" H 7100 4800 30  0000 C CNN
	1    7100 4800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR068
U 1 1 54E61A15
P 7100 5100
F 0 "#PWR068" H 7100 5100 30  0001 C CNN
F 1 "GND" H 7100 5030 30  0001 C CNN
F 2 "" H 7100 5100 60  0000 C CNN
F 3 "" H 7100 5100 60  0000 C CNN
	1    7100 5100
	1    0    0    -1  
$EndComp
$Comp
L C C16
U 1 1 54E61B55
P 7450 4200
F 0 "C16" H 7450 4300 40  0000 L CNN
F 1 "10.0uF" H 7456 4115 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7488 4050 30  0001 C CNN
F 3 "" H 7450 4200 60  0000 C CNN
	1    7450 4200
	1    0    0    -1  
$EndComp
$Comp
L C C17
U 1 1 54E61C60
P 7750 4200
F 0 "C17" H 7750 4300 40  0000 L CNN
F 1 "10.0uF" H 7756 4115 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7788 4050 30  0001 C CNN
F 3 "" H 7750 4200 60  0000 C CNN
	1    7750 4200
	1    0    0    -1  
$EndComp
$Comp
L C C18
U 1 1 54E61C89
P 8050 4200
F 0 "C18" H 8050 4300 40  0000 L CNN
F 1 "10.0uF" H 8056 4115 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8088 4050 30  0001 C CNN
F 3 "" H 8050 4200 60  0000 C CNN
	1    8050 4200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR069
U 1 1 54E61FFD
P 7750 5100
F 0 "#PWR069" H 7750 5100 30  0001 C CNN
F 1 "GND" H 7750 5030 30  0001 C CNN
F 2 "" H 7750 5100 60  0000 C CNN
F 3 "" H 7750 5100 60  0000 C CNN
	1    7750 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4000 5000 4000
Connection ~ 4900 3900
Wire Wire Line
	3850 4100 3950 4100
Wire Wire Line
	3950 4100 3950 4400
Wire Wire Line
	5000 4100 4900 4100
Wire Wire Line
	4900 4100 4900 4400
Connection ~ 4350 3900
Wire Wire Line
	4350 4350 4350 4400
Wire Wire Line
	4350 3850 4350 3950
Wire Wire Line
	4300 3900 5000 3900
Wire Wire Line
	5900 4000 5950 4000
Wire Wire Line
	5900 3900 6450 3900
Wire Wire Line
	6350 4000 6400 4000
Wire Wire Line
	6400 4000 6400 3900
Connection ~ 6400 3900
Wire Wire Line
	7050 3900 8400 3900
Wire Wire Line
	7100 3900 7100 3950
Wire Wire Line
	7100 4450 7100 4550
Wire Wire Line
	5900 4100 5950 4100
Wire Wire Line
	5950 4100 5950 4500
Wire Wire Line
	7100 5050 7100 5100
Wire Wire Line
	8050 4500 8050 4400
Wire Wire Line
	7750 4400 7750 5100
Connection ~ 7750 4500
Wire Wire Line
	7450 4400 7450 4500
Wire Wire Line
	7450 3900 7450 4000
Connection ~ 7100 3900
Wire Wire Line
	7750 3900 7750 4000
Connection ~ 7450 3900
Wire Wire Line
	8050 3900 8050 4000
Connection ~ 7750 3900
Wire Wire Line
	7450 4500 8050 4500
Wire Wire Line
	5950 4500 7100 4500
Connection ~ 7100 4500
$Comp
L DIODESCH D3
U 1 1 54E6321D
P 4100 3900
F 0 "D3" H 4100 4000 40  0000 C CNN
F 1 "DIODESCH" H 4100 3800 40  0000 C CNN
F 2 "Custom_SMD:SOD123" H 4100 3900 60  0001 C CNN
F 3 "" H 4100 3900 60  0000 C CNN
	1    4100 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3900 3900 3900
Text HLabel 9300 3900 2    60   Output ~ 0
+5V
Connection ~ 8050 3900
Text HLabel 8400 4950 2    60   Output ~ 0
GND
Wire Wire Line
	7750 4950 8400 4950
Connection ~ 7750 4950
NoConn ~ 3850 4000
$Comp
L PWR_FLAG #FLG070
U 1 1 54E7BC9F
P 4350 3850
F 0 "#FLG070" H 4350 3945 30  0001 C CNN
F 1 "PWR_FLAG" H 4350 4030 30  0000 C CNN
F 2 "" H 4350 3850 60  0000 C CNN
F 3 "" H 4350 3850 60  0000 C CNN
	1    4350 3850
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG071
U 1 1 54E7C177
P 9050 3850
F 0 "#FLG071" H 9050 3945 30  0001 C CNN
F 1 "PWR_FLAG" H 9050 4030 30  0000 C CNN
F 2 "" H 9050 3850 60  0000 C CNN
F 3 "" H 9050 3850 60  0000 C CNN
	1    9050 3850
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG072
U 1 1 54E7C27E
P 8050 4900
F 0 "#FLG072" H 8050 4995 30  0001 C CNN
F 1 "PWR_FLAG" H 8050 5080 30  0000 C CNN
F 2 "" H 8050 4900 60  0000 C CNN
F 3 "" H 8050 4900 60  0000 C CNN
	1    8050 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4900 8050 4950
Connection ~ 8050 4950
$Comp
L C C13
U 1 1 54E8CF2B
P 4650 4150
F 0 "C13" H 4650 4250 40  0000 L CNN
F 1 "0.1uF" H 4656 4065 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4688 4000 30  0001 C CNN
F 3 "" H 4650 4150 60  0000 C CNN
	1    4650 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR073
U 1 1 54E8CF70
P 4650 4400
F 0 "#PWR073" H 4650 4400 30  0001 C CNN
F 1 "GND" H 4650 4330 30  0001 C CNN
F 2 "" H 4650 4400 60  0000 C CNN
F 3 "" H 4650 4400 60  0000 C CNN
	1    4650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4350 4650 4400
Wire Wire Line
	4650 3900 4650 3950
Connection ~ 4650 3900
$Comp
L FUSE F1
U 1 1 54E8D621
P 8650 3900
F 0 "F1" H 8750 3950 40  0000 C CNN
F 1 "1.0A" H 8550 3850 40  0000 C CNN
F 2 "Capacitors_SMD:C_1812_HandSoldering" H 8650 3900 60  0001 C CNN
F 3 "" H 8650 3900 60  0000 C CNN
	1    8650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3900 9300 3900
Connection ~ 9050 3900
Wire Wire Line
	9050 3850 9050 3900
Wire Wire Line
	4900 3700 4900 4000
$Comp
L MCP6541 U7
U 1 1 54E8F8F5
P 7500 2600
F 0 "U7" H 7700 2900 70  0000 C CNN
F 1 "MCP6541" H 7700 2800 70  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT23-5" H 7500 2600 60  0001 C CNN
F 3 "" H 7500 2600 60  0000 C CNN
	1    7500 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR074
U 1 1 54E8F9BA
P 7400 3050
F 0 "#PWR074" H 7400 3050 30  0001 C CNN
F 1 "GND" H 7400 2980 30  0001 C CNN
F 2 "" H 7400 3050 60  0000 C CNN
F 3 "" H 7400 3050 60  0000 C CNN
	1    7400 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3000 7400 3050
Text Label 8400 4200 0    60   ~ 0
Local_5V
Text Label 7450 1850 0    60   ~ 0
Local_5V
Wire Wire Line
	7400 2200 7400 1850
Wire Wire Line
	7200 1850 7450 1850
$Comp
L C C15
U 1 1 54E8FE71
P 7200 2100
F 0 "C15" H 7200 2200 40  0000 L CNN
F 1 "0.1uF" H 7206 2015 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7238 1950 30  0001 C CNN
F 3 "" H 7200 2100 60  0000 C CNN
	1    7200 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 1850 7200 1900
Connection ~ 7400 1850
Wire Wire Line
	7200 2300 7200 2350
$Comp
L GND #PWR075
U 1 1 54E900A7
P 7200 2350
F 0 "#PWR075" H 7200 2350 30  0001 C CNN
F 1 "GND" H 7200 2280 30  0001 C CNN
F 2 "" H 7200 2350 60  0000 C CNN
F 3 "" H 7200 2350 60  0000 C CNN
	1    7200 2350
	1    0    0    -1  
$EndComp
Text HLabel 5000 2900 0    60   Input ~ 0
USB_VCC
Wire Wire Line
	5000 2900 5200 2900
Text Label 5200 2900 0    60   ~ 0
USB_VCC
$Comp
L FDN340P Q11
U 1 1 54E914D8
P 8450 2900
F 0 "Q11" V 8300 2650 40  0000 C CNN
F 1 "FDN340P" V 8650 2800 40  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT23" H 8450 2900 60  0001 C CNN
F 3 "" H 8450 2900 60  0000 C CNN
	1    8450 2900
	0    1    1    0   
$EndComp
Text Label 8000 2950 2    60   ~ 0
USB_VCC
Wire Wire Line
	8000 2950 8150 2950
Wire Wire Line
	8000 2600 8450 2600
Wire Wire Line
	8450 2600 8450 2650
Wire Wire Line
	8600 2950 8700 2950
Text Label 8700 2950 0    60   ~ 0
Local_5V
Wire Wire Line
	8400 3900 8400 4200
Wire Wire Line
	4900 3700 5000 3700
Text Label 5000 3700 0    60   ~ 0
Barrel_Rectified
Text Label 6400 2500 2    60   ~ 0
Barrel_Rectified
$Comp
L R R11
U 1 1 54E9260E
P 6700 2500
F 0 "R11" V 6780 2500 40  0000 C CNN
F 1 "30k" V 6707 2501 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6630 2500 30  0001 C CNN
F 3 "" H 6700 2500 30  0000 C CNN
	1    6700 2500
	0    1    1    0   
$EndComp
$Comp
L R R13
U 1 1 54E92677
P 6950 2200
F 0 "R13" V 7030 2200 40  0000 C CNN
F 1 "10k" V 6957 2201 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 2200 30  0001 C CNN
F 3 "" H 6950 2200 30  0000 C CNN
	1    6950 2200
	-1   0    0    1   
$EndComp
$Comp
L R R12
U 1 1 54E92829
P 6700 2700
F 0 "R12" V 6780 2700 40  0000 C CNN
F 1 "16k" V 6707 2701 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6630 2700 30  0001 C CNN
F 3 "" H 6700 2700 30  0000 C CNN
	1    6700 2700
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 54E9282F
P 6950 3000
F 0 "R14" V 7030 3000 40  0000 C CNN
F 1 "10k" V 6957 3001 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6880 3000 30  0001 C CNN
F 3 "" H 6950 3000 30  0000 C CNN
	1    6950 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 1950 7050 1950
Wire Wire Line
	7050 1950 7050 2000
$Comp
L GND #PWR076
U 1 1 54E92A36
P 7050 2000
F 0 "#PWR076" H 7050 2000 30  0001 C CNN
F 1 "GND" H 7050 1930 30  0001 C CNN
F 2 "" H 7050 2000 60  0000 C CNN
F 3 "" H 7050 2000 60  0000 C CNN
	1    7050 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR077
U 1 1 54E92A55
P 6950 3300
F 0 "#PWR077" H 6950 3300 30  0001 C CNN
F 1 "GND" H 6950 3230 30  0001 C CNN
F 2 "" H 6950 3300 60  0000 C CNN
F 3 "" H 6950 3300 60  0000 C CNN
	1    6950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3250 6950 3300
Wire Wire Line
	6950 2700 6950 2750
Wire Wire Line
	6950 2700 7000 2700
Wire Wire Line
	6950 2500 7000 2500
Wire Wire Line
	6950 2450 6950 2500
Wire Wire Line
	6400 2500 6450 2500
Wire Wire Line
	6450 2700 6400 2700
Text Label 6400 2700 2    60   ~ 0
USB_VCC
Wire Notes Line
	9350 1700 9350 3400
Wire Notes Line
	9350 3400 4450 3400
Wire Notes Line
	4450 3400 4450 1700
Wire Notes Line
	4450 1700 9350 1700
Text Notes 4500 1650 0    60   ~ 0
Voltage Source Selection\nIf barrel jack voltage is less than 7.69 (5V / 0.65), USB source will be used.\nIf only the USB is plugged in, current will initially flow through the FET, \npowering the system including the comparator. The regulator will output\n0V since USB_VCC > 0V. The FET will open and allow all of USB_VCC to pass without a voltage drop.
$EndSCHEMATC
