EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Custom_Logic
LIBS:Custom_Transistors
LIBS:Custom_EEPROM
LIBS:Custom_Passives
LIBS:Custom_IC_Power
LIBS:opendous
LIBS:Custom_Analog_IC
LIBS:Lightboard-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 6
Title "Lightboard Matirx"
Date "Wednesday, March 18, 2015"
Rev "3"
Comp "Delta Charlie Mike"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5850 4750 0    60   UnSpc ~ 0
UCAP
Text HLabel 5850 4200 0    60   BiDi ~ 0
D+
Text HLabel 5850 4050 0    60   Input ~ 0
D-
Text HLabel 5850 4350 0    60   Input ~ 0
UID
$Comp
L GND #PWR052
U 1 1 54E5E22C
P 6700 5250
F 0 "#PWR052" H 6700 5250 30  0001 C CNN
F 1 "GND" H 6700 5180 30  0001 C CNN
F 2 "" H 6700 5250 60  0000 C CNN
F 3 "" H 6700 5250 60  0000 C CNN
	1    6700 5250
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 54E5E240
P 6700 5000
F 0 "C9" H 6700 5100 40  0000 L CNN
F 1 "1uF" H 6706 4915 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6738 4850 30  0001 C CNN
F 3 "" H 6700 5000 60  0000 C CNN
	1    6700 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5200 6700 5250
Wire Wire Line
	6700 4800 6700 4750
Wire Wire Line
	6700 4750 5850 4750
$Comp
L R R6
U 1 1 54E5E2C3
P 6150 4050
F 0 "R6" V 6230 4050 40  0000 C CNN
F 1 "22" V 6157 4051 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6080 4050 30  0001 C CNN
F 3 "" H 6150 4050 30  0000 C CNN
	1    6150 4050
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 54E5E321
P 6150 4200
F 0 "R7" V 6230 4200 40  0000 C CNN
F 1 "22" V 6157 4201 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6080 4200 30  0001 C CNN
F 3 "" H 6150 4200 30  0000 C CNN
	1    6150 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 4200 6700 4200
Wire Wire Line
	6400 4050 6450 4050
Wire Wire Line
	6450 4050 6450 4100
Wire Wire Line
	6450 4100 6700 4100
Wire Wire Line
	5850 3900 6500 3900
Wire Wire Line
	6500 3850 6500 4000
Wire Wire Line
	6500 4000 6700 4000
Wire Wire Line
	5850 4350 6450 4350
Wire Wire Line
	6450 4350 6450 4300
Wire Wire Line
	6450 4300 6700 4300
Wire Wire Line
	5850 4500 6500 4500
Wire Wire Line
	6500 4500 6500 4400
Wire Wire Line
	6500 4400 6700 4400
Wire Wire Line
	5850 4050 5900 4050
Wire Wire Line
	5850 4200 5900 4200
$Comp
L C C8
U 1 1 54E5F3CB
P 6050 3650
F 0 "C8" H 6050 3750 40  0000 L CNN
F 1 "10uF" H 6056 3565 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6088 3500 30  0001 C CNN
F 3 "" H 6050 3650 60  0000 C CNN
	1    6050 3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR053
U 1 1 54E5F434
P 6250 3600
F 0 "#PWR053" H 6250 3600 30  0001 C CNN
F 1 "GND" H 6250 3530 30  0001 C CNN
F 2 "" H 6250 3600 60  0000 C CNN
F 3 "" H 6250 3600 60  0000 C CNN
	1    6250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3900 6050 3850
Connection ~ 6050 3900
Wire Wire Line
	6050 3450 6050 3400
Wire Wire Line
	6050 3400 6250 3400
Wire Wire Line
	6250 3400 6250 3600
Text HLabel 5850 4500 0    60   UnSpc ~ 0
UVSS
$Comp
L GND #PWR054
U 1 1 54E797F2
P 6050 4600
F 0 "#PWR054" H 6050 4600 30  0001 C CNN
F 1 "GND" H 6050 4530 30  0001 C CNN
F 2 "" H 6050 4600 60  0000 C CNN
F 3 "" H 6050 4600 60  0000 C CNN
	1    6050 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4600 6050 4500
Connection ~ 6050 4500
Wire Wire Line
	7700 4000 7800 4000
Wire Wire Line
	7800 4000 7800 4400
Wire Wire Line
	7800 4100 7700 4100
Wire Wire Line
	7800 4300 7700 4300
Wire Wire Line
	7800 4400 7700 4400
Connection ~ 7800 4300
Connection ~ 7800 4100
Wire Wire Line
	7800 4200 7900 4200
Wire Wire Line
	7900 4200 7900 4250
Connection ~ 7800 4200
$Comp
L GND #PWR055
U 1 1 54E7B0FD
P 7900 4800
F 0 "#PWR055" H 7900 4800 30  0001 C CNN
F 1 "GND" H 7900 4730 30  0001 C CNN
F 2 "" H 7900 4800 60  0000 C CNN
F 3 "" H 7900 4800 60  0000 C CNN
	1    7900 4800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG056
U 1 1 54E7CC4E
P 6500 3850
F 0 "#FLG056" H 6500 3945 30  0001 C CNN
F 1 "PWR_FLAG" H 6500 4030 30  0000 C CNN
F 2 "" H 6500 3850 60  0000 C CNN
F 3 "" H 6500 3850 60  0000 C CNN
	1    6500 3850
	1    0    0    -1  
$EndComp
Connection ~ 6500 3900
Text HLabel 5850 5600 0    60   UnSpc ~ 0
UVCC
Wire Wire Line
	5850 5600 5950 5600
Wire Wire Line
	5950 5600 5950 5500
$Comp
L VCC #PWR057
U 1 1 54E7D959
P 5950 5500
F 0 "#PWR057" H 5950 5600 30  0001 C CNN
F 1 "VCC" H 5950 5600 30  0000 C CNN
F 2 "" H 5950 5500 60  0000 C CNN
F 3 "" H 5950 5500 60  0000 C CNN
	1    5950 5500
	1    0    0    -1  
$EndComp
Text HLabel 5850 3900 0    60   UnSpc ~ 0
VBUS
$Comp
L USB-MINI-B J6
U 1 1 54EB8A67
P 7200 4200
F 0 "J6" H 6900 4550 50  0000 C CNN
F 1 "USB-MINI-B" H 7050 3850 50  0000 C CNN
F 2 "Personal_SMD:Hirose-MiniUSB-UX60-MB-5S8" H 7200 4100 50  0001 C CNN
F 3 "" H 7200 4100 50  0000 C CNN
	1    7200 4200
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 54EB8AFB
P 7900 4500
F 0 "R8" V 7980 4500 40  0000 C CNN
F 1 "1M" V 7907 4501 40  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7830 4500 30  0001 C CNN
F 3 "" H 7900 4500 30  0000 C CNN
	1    7900 4500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7900 4750 7900 4800
$EndSCHEMATC
